  <!-- Scripts -->
    <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> Copyright Hosting Usage Portal. All Rights Reserved 
               <!--  <a target="_blank" href="http://keenthemes.com"></a> &nbsp;&nbsp; -->
               <!--  <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">
                    <a> -->

                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
            <!-- BEGIN QUICK NAV -->
          
            <div class="quick-nav-overlay"></div>
            <!-- END QUICK NAV -->
            <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
            <!-- BEGIN CORE PLUGINS -->
          
            <!-- BEGIN CORE PLUGINS -->
           
            <script src="{{url('/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
            <!-- END CORE PLUGINS -->
            <!-- BEGIN PAGE LEVEL PLUGINS -->
            <script src="{{url('/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
            <!-- END PAGE LEVEL PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN PAGE LEVEL SCRIPTS -->
            <script src="{{url('/assets/pages/scripts/table-datatables-responsive.min.js')}}" type="text/javascript"></script>
            <!-- END PAGE LEVEL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <script src="{{url('/assets/layouts/layout2/scripts/layout.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/layouts/layout2/scripts/demo.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
            <script src="{{url('/assets/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>
           
            <script src="{{url('/assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}" type="text/javascript"></script> <!-- END THEME LAYOUT SCRIPTS -->
            <!-- END THEME LAYOUT SCRIPTS -->
              
  