<!DOCTYPE html>
<html lang ="{{ app()->getLocale() }}">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
   .error{ color:red; } 
  </style>
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

@include('user.includes.header')
<div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
@include('user.includes.sidebar')
</div>
                <!-- END SIDEBAR -->
            </div>
<div class="page-content-wrapper">
                <div class="page-content">
	@yield('content')
	</div>
</div>
@include('user.includes.footer')

</body>

</html>
