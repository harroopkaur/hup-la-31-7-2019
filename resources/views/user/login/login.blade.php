<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ app()->getLocale() }}">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        
        <title> User Login </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #2 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{URL::to('/')}}/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
          <link rel="stylesheet" href="{{url('assets/landing/main.css')}}">
        <link href="{{URL::to('/')}}/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{URL::to('/')}}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{URL::to('/')}}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
       <link href="{{URL::to('/')}}/public/css/admin/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link rel="stylesheet" href="{{url('/css/style.css')}}">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{url('/assets/apps/styles/progress-tracker.css')}}">
         <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
       </head>
        <style>
        .login-content .truck_logo{
        width:48%;
        }
        </style>
    <!-- END HEAD -->

    <body>
        
<!-- ================================= new header start ======================== -->
 <div class="overlapsty95">
  <div class="container">
    <div class="row">
<div class="col-md-12 col-sm-12 col-lg-12 main-headersty">
  <div class="col-md-4 wel-comemessage">
<!-- <img class="logo-topsty" src="{{url('assets/landing/img/logo.png')}}"> -->
<p>{{ __('messages.welcome to') }} Hosting Usage Portal</p>
</div>
<div class="col-md-6">
<ul class="top_contlinks">
                        <li><a><!-- <i class="fa fa-phone"></i> --><img class="top-iconsty951" src="{{url('assets/landing/img/colo-phone.png')}}"> (305)851-3545</a></li>
                        <li><a href="mailto:info@licensingassurance.com" title="info@licensingassurance.com"><!-- <i class="fa fa-envelope"></i> --> <img class="top-iconsty951" src="{{url('assets/landing/img/color-email.png')}}"> info@licensingassurance.com</a></li>
                      
            
          
                    </ul>
</div>
<div class="col-md-2 selctlang-sty">

<select class="lang-iconchange" name="forma" onchange="location = this.value;">
<option value="">{{ __('messages.select Language') }}</option>
<option value="{{ url('locale/en') }}">ENGLISH</option>
<option value="{{ url('locale/es') }}">SPANISH</option>
</select>
</div>
</div>
</div>
</div>

<!-- ===============================slider data start================== -->
<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
     
    </div>

    <div class="collapse navbar-collapse center-datamenu pading-zerosty" id="myNavbar">
       <div class="navbar-header">
      <a class="navbar-brand logo-substy" href="{{url('')}}"><img class="logo-topsty whitelogo-show" src="{{url('assets/landing/img/logo-white.png')}}"> <img class="logo-topsty planlogo-show" src="{{url('assets/landing/img/logo.png')}}"></a>
    </div>
      <ul class="nav navbar-nav uldatacenter-menuy951">
        <li><a href="{{url('')}}"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorhome.png')}}">
       {{ __('messages.Menu one') }}
       </a></li>        
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/color-web.png')}}"> 
      {{ __('messages.Menu two') }}
      </a></li>
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/coloridea.png')}}"> {{ __('messages.Menu three') }}
        </a></li>
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorcontact.png')}}"> {{ __('messages.Menu four') }} 
        </a></li>
        <li><a href="#" onclick="openNav()"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorlogin.png')}}"> 
        {{ __('messages.Menu five') }}
      </a></li>
      </ul>
     
    </div>
  </div>
</nav>
  </div>
<!-- ================================== new header close ================= -->


<!-- ===============================slider data start================== -->
  
  <!-- ================================= -->
  <div id="mySidebar" class="sidebar">
<div class="pop-closesty951">
  <span class="closebtn" onclick="closeNav()">×</span>
</div>
<div class="login-maindatahad">
    <p>{{ __('messages.login') }}</p> 
</div>
<div class="main-popdataleft">
  <ul>
      <li><a href="{{url('dcsection/')}}" class="login-s">Log In DC</a></li>
      <li><a href="{{url('user/')}}" class="login-s">Log In End User</a></li>
      <li><a href="{{url('admin/')}}" class="login-s">Log In LA</a></li>
  </ul>
</div>
</div>
<!-- ===================================== -->

 <div class="main-topbanner951">
  <div class="main-layer951data">
    <div class="container">
      <div class="row">
    <h6>End User</h6>
  </div>
  </div>
  </div>
 </div>

<!-- =====================slider data close ======================== -->

<!-- ========================= -->
<div class="end-usersec1">
  <div class="container">
    <div class="row">

<div class="col-md-7  col-sm-7 col-lg-7 box_2">
  <legend class="process-flowsty951">
{{ __('messages.process Flow') }} </legend>
<div class="col-md-12 te_center">
     <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
         <img src="{{url('/assets/landing/img/password.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4>
             
            </span>
            <span class="progress-marker">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
            <img src="{{url('/assets/landing/img/file.png')}}">
              <h4 class="progress-title">{{ __('messages.assigned License') }}
</h4>
          
            </span>
            <span class="progress-marker">2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
     <img src="{{url('/assets/landing/img/literature.png')}}">
              <h4 class="progress-title">{{ __('messages.send to DC update') }}
</h4>
              
            </span>
            <span class="progress-marker">3</span>
          </li>
        </ul>  
		</div>
</div>
<div class="col-md-4  col-md-offset-1 col-sm-4 col-lg-4 box_4">
  <div class="form-maindatabg951">
    <div class="form-loginsty951">
    <h6> {{ __('messages.log In') }} </h6>
  </div>
<div class="col-md-12">
<form action="{{ url('/user/login') }}" class="login-form" method="post" role="form">
                            {{ csrf_field() }}
     <div class="input-group login-forminputsty form-marsty951 {{ $errors->has('Usuario') ? ' has-error' : '' }}">
      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
      <input class="form-control" type="text" id="Usuario" placeholder="{{ __('messages.email') }}" name="email" value="{{ old('email') }}" autofocus> 
      @if ($errors->has('Usuario'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Usuario') }}</strong>
                                        </span>
                                    @endif
    </div>
    
    <div class="input-group login-forminputsty form-marsty951 {{ $errors->has('Contraseña') ? ' has-error' : '' }}">
      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
      <input class="form-control" type="password" id="Contraseña" placeholder="{{ __('messages.password') }}" name="password"> 
      @if ($errors->has('Contraseña'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('Contraseña') }}</strong>
                                        </span>
                                    @endif   </div>
    
    
    


<!--  <center><h6 style="color:#271c5c;">{{ __('messages.forgot your password?') }}
</h6></center> -->
<center> <button type="submit" class="login-dcsty">{{ __('messages.login') }}
</button>
</center>

<div class="create-newacountsty951">
  <a href="#">{{ __('messages.forgot your password?') }}</a>
</div>

</form>
</div>
<div class="clearfix"></div>
</div>
</div>
<!-- ======================================== -->
</div>
</div>
</div>
<!-- =================================== -->
<div class="end-usersec2">
  <div class="container">
    <div class="'row">

<div class="col-md-12 col-sm-12 col-lg-12">

  <legend class="process-flowsty951">
 {{ __('messages.manage your Information in Seconds') }}
 </legend>


<div class="col-md-12 col-sm-12 col-lg-12 ">
  <div class="row text-center slideanim">
    <div class="col-md-4">
      <div class="cal-datafooter951">
        <img src="{{url('/assets/landing/img/calendar-image.png')}}" alt="Paris" >
 
        <h4>{{ __('messages.track expiration of Contracts and Notifications') }}</h4>
       </div>
     </div>
     
    <div class="col-md-4">
    <div class="cal-datafooter951">   
        <img src="{{url('/assets/landing/img/graff655.png')}}" alt="New York" >
        <h4>{{ __('messages.see reports and graphics of your VMs') }}</h4>
      </div>
    </div>

    <div class="col-md-4">
    <div class="cal-datafooter951"> 
        <img src="{{url('/assets/landing/img/graffs2.png')}}" alt="San Francisco" >
        <h4>{{ __('messages.send your update to your provider') }}</h4> 

    </div>
  </div>

  </div>
</div>



</div>
</div>
</div>
</div>
<!-- ============================================== -->


<!-- ======================== new footer  =================== -->

<div class="mail-us"><span class="mailusprt1"> {{ __('messages.mail Requirement') }} :</span> <a href="mailto:info@licensingassurance.com"><i class="fa fa-paper-plane"></i>info@licensingassurance.com</a> <span>|</span> <span class="mailusprt2">{{ __('messages.call us') }} :</span> <a href="tel:(305)851-3545"><i class="fa fa-phone"></i>(305)851-3545</a></div>

<footer>
<div class="sub-footer">
<ul>
<li><a href="#">{{ __('messages.dedicated Server Hosting') }} </a></li>
<li><a href="#">{{ __('messages.vPS Hosting') }} </a></li>
<li><a href="#">{{ __('messages.cloud Hosting') }} </a></li>
<li><a href="#">{{ __('messages.server Colocation') }} </a></li>
<li><a href="#">{{ __('messages.data Center') }} </a></li>
<li><a href="#">{{ __('messages.email Server Hosting') }} </a></li>
<li><a href="#">{{ __('messages.application Hosting') }}</a></li>
<li><a href="#">{{ __('messages.domain Registration') }} </a></li>
<li><a href="#">{{ __('messages.sSL Certificates') }} </a></li>
</ul>
</div>
<div class="footer-menu row">
<div class="col-sm-6">
<ul>
<li><a href="#"> {{ __('messages.knowledgebase') }}</a></li>
<li><a href="#"> {{ __('messages.wiki') }} </a></li>
<li><a href="#"> {{ __('messages.forum') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.blog') }} </a></li>
<li><a href="#"> {{ __('messages.resource Library') }} </a></li>
<li><a href="#"> {{ __('messages.clients') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.review') }} </a></li>
<li><a href="#"> {{ __('messages.privacy Policy') }} </a></li>
<li><a href="#"> {{ __('messages.terms Conditions') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.fup Policy') }} </a></li>
<li><a href="#"> {{ __('messages.disclaimer') }} </a></li>
<li><a href="#"> {{ __('messages.xml') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.contact Us') }} </a></li>
<li><a href="#"> {{ __('messages.sitemap') }}</a></li>
</ul>
</div>
<div class="col-sm-6 row">
<div class="col-sm-6 col-xs-12 response">
<strong> {{ __('messages.quick Response') }} </strong>
<a href="info@licensingassurance.com">info@licensingassurance.com</a>
<div class="experts">
<strong> {{ __('messages.speak to Sales Tech Experts') }} </strong>
<a href="(305)851-3545">(305)851-3545</a>
</div>
</div>
<div class="col-sm-6 col-xs-12">
<strong> {{ __('messages.live Chat') }} </strong>
 {{ __('messages.talk to our Technical Sales Representative') }} 
<div class="copy">
© <script>document.write(new Date().getFullYear())</script> Hosting Usage Portal. {{ __('messages.all Rights Reserved') }} .
</div>
</div>
</div>
<!-- <div class="col-sm-12 col-md-12 col-xs-12">
<div style="margin-left:5px; float:left;" class="textfooterimg-right">
<span xmlns:v="http://rdf.data-vocabulary.org/#" typeof="v:Review-aggregate">
<span property="v:itemreviewed" style="color:#666;">Go4Hosting</span>
<span rel="v:rating" style="color:#666;">is rated
<span typeof="v:Rating" style="color:#666;">
<span property="v:average">4.25</span> /
<span property="v:best">5</span>
</span>
</span><span style="color:#666;">in</span>
<span property="v:count" style="color:#666;">45</span> <span style="color:#666;">reviews on</span> <a rel="nofollow" href="https://www.hostreview.com/companies/go4hosting/reviews" style="color:#fff!important;">Host Review</a>.
</span>
</div>
</div> -->
</div>
<div class="payment-bar row">
<div class="col-sm-5 social">
<ul>
<li>{{ __('messages.go Social') }} </li>
<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-telegram"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
</ul>
</div>
<div class="col-sm-6 payment">
<ul>
<li>{{ __('messages.we Accept') }} 
<strong>{{ __('messages.cards cheques and online transfer') }} </strong></li>
<li><img src="{{url('assets/landing/img/maestro.png')}}" width="42" height="26"></li>
<li><img src="{{url('assets/landing/img/master-card.png')}}" width="42" height="26"></li>
<li><img src="{{url('assets/landing/img/footer-icon.png')}}" width="41" height="41"></li>
<li><img src="{{url('assets/landing/img/visa.png')}}" width="50" height="16"></li>
<li><img src="{{url('assets/landing/img/transfer.png')}}" width="37" height="32"></li>
</ul>
</div>
</div>

</footer>
<!-- ========================= new footer close =========================== -->

</body>
</html>
        
        
        
        
        
        
      
      


       


        <!-- END : LOGIN PAGE 5-1 -->
        <!--[if lt IE 9]>
<script src="{{URL::to('/')}}/assets/global/plugins/respond.min.js"></script>
<script src="{{URL::to('/')}}/assets/global/plugins/excanvas.min.js"></script> 
<script src="{{URL::to('/')}}/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{URL::to('/')}}/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
       
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{URL::to('/')}}/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{URL::to('/')}}/assets/pages/scripts/login-5.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script src="{{URL::to('/')}}/assets/apps/scripts/site.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script>
$(window).scroll(function(){
    if ($(window).scrollTop() >= 300) {
        $('.navbar-inverse').addClass('fixed-header');
    }
    else {
        $('.navbar-inverse').removeClass('fixed-header');
    }
});
</script>

<script>
function openNav() {
  document.getElementById("mySidebar").style.width = "350px";
  document.getElementById("main").style.marginLeft = "350px";
  document.getElementById("mySidebar").style.marginRight= "10px;";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  document.getElementById("mySidebar").style.marginRight= "-8px;";
}
</script>
    </body>

</html>