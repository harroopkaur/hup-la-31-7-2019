@extends('user.includes.main')

@section('content')
   
<!-- END STYLE CUSTOMIZER -->
<!-- BEGIN PAGE HEADER-->

<style type="text/css">
  .aliggn{
    vertical-align:middle!important;
  }
</style>

<div class="bg-whitesty961">
    <h4 class="font-sizemainhad986">{{ __('messages.vM Inventory') }}</h4>
    <div class="page-toolbar"></div>

<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS -->

@if (session('success'))
<div class="alert alert-success">
	{{ session('success') }}
</div>
@endif
@if (session('d_success'))
<div class="alert alert-success">
	{{ session('d_success') }}
</div>aliggn
@endif
  
<div class="actions">
 
       <a href="{{url('/user/vminventory')}}" class="btn btn-lg default refresh-custbutsty985" > <i class="fa fa-refresh" id="ic"></i> {{ __('messages.refresh info') }}</a> 
   
           
       </div>
<br></br>



<div class="portlet" >
                               
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                    
                                        <table class="table table-striped table-bordered table-hover table-imgtop">
                                            <thead>
                                        <th colspan="8" style="text-align: center;font-size:20px;color: #fff;background: #62008c;">{{ __('messages.datacenter') }}</th>
                                        <th colspan="11" style="text-align: center;font-size:20px;color: #fff;background: #bf2476;">{{ __('messages.endUsers') }}</th>
                                                           
                                                <tr>
                                                    <th class="text-center aliggn"> {{ __('messages.sr.No.') }}</th>
                                                  <!--  <th> [] &nbsp;&nbsp;</th>-->
                                                    <th class="text-center aliggn">{{ __('messages.endUsers') }}&nbsp;&nbsp; </th>
                                                    <th class="text-center aliggn width-updatedate">{{ __('messages.update data') }} </th>
                                                    <th class="text-center aliggn"> {{ __('messages.vm') }} &nbsp;&nbsp;</th>
                                                    <th class="text-center aliggn">{{ __('messages.cores') }}&nbsp;&nbsp; </th>
                                                    <th class="text-center aliggn"> {{ __('messages.procs') }}&nbsp;&nbsp; </th>
                                                    <th class="text-center aliggn">{{ __('messages.date install') }} </th>
                                                    <th class="text-center aliggn"><img  src="{{url('/media/img1/sql.png')}}" alt="sql"/><br>{{ __('messages.sql') }}</th>
                                                    <th class="text-center aliggn"><img  src="{{url('/media/img1/office.png')}}" alt="office"/><br>{{ __('messages.office') }}</th>
                                                    <th class="text-center aliggn"><img  src="{{url('/media/img1/sharepoint.png')}}"alt="logo"/><br>{{ __('messages.sharepoint') }}</th>
                                                    <th class="text-center aliggn"> <img  src="{{url('/media/img1/exchange.png')}}"alt="logo"/><br>{{ __('messages.exchange') }}</th>
                                                    <th class="text-center aliggn"><img  src="{{url('/media/img1/skype.png')}}" alt="logo"/><br>{{ __('messages.skypebiz') }}</th>
                                                    <th class="text-center aliggn"> <img  src="{{url('/media/img1/visualstudio.png')}}"alt="logo"/><br>{{ __('messages.visual Studio') }}</th>
                                                    <th class="text-center aliggn"><img  src="{{url('/media/img1/dynamic.png')}}"alt="logo"/><br>{{ __('messages.dynamic') }}</th>
                                                    <th class="text-center aliggn"> <img  src="{{url('/media/img1/project.png')}}"alt="logo"/><br>{{ __('messages.project') }}</th>
                                                    <th class="text-center aliggn"><img  src="{{url('/media/img1/rds.png')}}" alt="logo"/><br>{{ __('messages.rds') }}</th>
                                                    <th class="text-center aliggn"><img  src="{{url('/media/img1/vision.png')}}" alt="logo"/><br>{{ __('messages.visio') }}</th>
                                                <th class="aliggn">{{ __('messages.action') }}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                            $i = 1
                                             @endphp
                                             @foreach($invent as $data1)  
                                   <form action = "{{url('user/vminventory/update',$data1->id)}}" id="formId" class="form-horizontal" enctype="multipart/form-data" method="POST">
					                {{ csrf_field() }}
                                    
                                <input type="hidden" name="id"value="{{$data1->id}}">
                                    <input type="hidden" name="dcid"value="{{$data1->dc_id}}">
                                      <input type="hidden" name="id"value="{{$data1->id}}">
                                                <tr>
                                               
                                                    <td>  {{  $i++ }} </td>
                                                    <!--<td class="text-center" ><i class="fa fa-send"></i>SendRequest </td>-->
                                                    <td class="text-center"> {{ $data1->name }} </td>
                                                    <td class="text-center"> {{date('d-M-Y',strtotime($data1->updatedate))}} </td>
                                                    <td class="text-center"> {{$data1->vm}} </td>
                                                    <td class="text-center"> {{$data1->cores}} </td>
                                                    <td class="text-center"> {{$data1->procs}} </td>
                                                    <td class="text-center"> {{date('d-M-Y',strtotime($data1->dateinstall))}} </td>
                                                    
                                                   <td > 
                                                    <select class="bs-select btn" name="sql" id="sql" >

                                                      @if($data1->sql)
                                                      <option value="{{$data1->sql}}">{{$data1->sql}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif    
                                                    @foreach($vminvent as $data)
                                                     @if($data->sql)
                                                        <option value="{{$data->sql}}">{{$data->sql}} </option>
                                                        @endif
                                                        @endforeach  
                                                    </select>
                                               </td>
 <td> 
                                                    <select class="bs-select btn" name="office" id="office"  >
                                                      @if($data1->office)
                                                      <option value="{{$data1->office}}">{{$data1->office}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif 
                                                    @foreach($vminvent as $data)
                                                     @if($data->office)
                                                        <option  value="{{$data->office}}">{{$data->office}} </option>
                                                      @endif
                                                        @endforeach
                                                    </select>
                                                 </td>
                                                    <td>
                                                    <select class="bs-select btn" name="sharepoint" id="sharepoint">
                                                          @if($data1->sharepoint)
                                                      <option value="{{$data1->sharepoint}}">{{$data1->sharepoint}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif 
                                                    @foreach($vminvent as $data)
                                                     @if($data->sharepoint)
                                                        <option value="{{$data->sharepoint}}">{{$data->sharepoint}} </option>
                                                        @endif
                                                        @endforeach  
                                                    </select>
                                                 </td>
                                                    <td>
                                                    <select class="bs-select btn" name="exchange" id="exchange">
                                                    @if($data1->exchange)
                                                      <option value="{{$data1->exchange}}">{{$data1->exchange}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif   
                                                    @foreach($vminvent as $data)
                                                     @if($data->exchange)
                                                        <option value="{{$data->exchange}}">{{$data->exchange}} </option>
                                                        @endif
                                                        @endforeach  
                                                    </select>
                                                  </td>
                                                    <td>
                                                    <select class="bs-select btn" name="skypebiz" id="skypebiz">
                                                     @if($data1->skypebiz)
                                                      <option value="{{$data1->skypebiz}}">{{$data1->skypebiz}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif 
                                                    @foreach($vminvent as $data)
                                                     @if($data->skypebiz)
                                                        <option value=" {{$data->skypebiz}} "> {{$data->skypebiz}}  </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                              </td>
                                                    <td> 
                                                    <select class="bs-select btn" name="visualstudio" id="visualstudio">
                                                         @if($data1->visualstudio)
                                                      <option value="{{$data1->visualstudio}}">{{$data1->visualstudio}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif 
                                                    @foreach($vminvent as $data)
                                                     @if($data->visualstudio)
                                                        <option value="{{$data->visualstudio}}">{{$data->visualstudio}} </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                               </td>
                                                    <td>
                                                    <select class="bs-select btn" name="dynamics" id="dynamics">
                                                     @if($data1->dynamics)
                                                      <option value="{{$data1->dynamics}}">{{$data1->dynamics}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif   
                                                    @foreach($vminvent as $data)
                                                     @if($data->dynamics)
                                                        <option value="{{$data->dynamics}}">{{$data->dynamics}}</option>
                                                    @endif
                                                        @endforeach
                                                    </select>
                                                 </td>
                                                    <td>
                                                    <select class="bs-select btn" name="project" id="project">
                                                       @if($data1->project)
                                                      <option value="{{$data1->project}}">{{$data1->project}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif    
                                                    @foreach($vminvent as $data)
                                                     @if($data->project)
                                                        <option value="{{$data->project}}">{{$data->project}} </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                 </td>
                                                    <td> 
                                                    <select class="bs-select btn" name="rds" id="rds">
                                                        @if($data1->rds)
                                                      <option value="{{$data1->rds}}">{{$data1->rds}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif    
                                                    @foreach($vminvent as $data)
                                                     @if($data->rds)
                                                    <option value="{{$data->rds}}">{{$data->rds}} </option>
                                                    @endif
                                                    @endforeach
                                                    </select>
                                                </td>
                                                    <td> 
                                                    <select class="bs-select btn" name="visio" id="visio">
                                                      @if($data1->visio)
                                                      <option value="{{$data1->visio}}">{{$data1->visio}}</option> 
                                                      @else
                                                         <option value="select">Select</option>
                                                      @endif     
                                                    @foreach($vminvent as $data)
                                                     @if($data->visio)
                                                        <option value="{{$data->visio}}">{{$data->visio}} </option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                </td>
                                                 <td>

                                                  <input type="submit"class="btn btn-sm btn-danger" name="submit" value="{{ __('messages.sent info') }}" id="one11"/>

                                                </td>  
                                             
                                            
  
                                                </tr>
                                                
                                                
                                               </form>

                                               @endforeach
                                            </tbody>
                                        </table>
                                 
                                    </div>
                                </div>
                            </div>
</div>                 



                                    


<!--sent info-->
<div id="sentinfo" class="modal fade circle" tabindex="-1" data-focus-on="input:first">

            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
            Sent info</h4>
            </div>
            <div class="modal-body">
            <div class="col-md-12">
        <img src="{{URL::asset('media/email.png')}}" alt="img" class="img-default" height="200px" width="100%"/>
        </div><br>
        <div class="col-md-12">
        <center><h4>Updated Information</h4></center>
        </div>
        <table border="3px solid" id="add" style="width:100%;">

        <thead>
      
        <tr class="heading">
<th>SQL</th>
<th>Office</th>
<th>Sharepoint</th>
<th>Exchange</th>
<th>SkypeBiz</th>
<th>VisualStudio</th>
<th>Dynamic</th>
<th>Project</th>
<th>RDS</th>
<th>Visio</th>
      
      </tr>
  
    </thead>

    <tbody class="apends">
         <!--  -->
</tbody>



</table>
                                       </div>
                                        <div class="modal-footer">
                                          
                                        <button type="submit" class="btn-circle" onClick='submitDetailsForm()' data-target="#sentinfo" data-toggle="modal">Send</button>
                                        <button type="button" class="btn-circle"  class="close" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                        </div>
                                    </div>
                                  

</div>
<div id="sendmail"></div>
<!-- scritp -->
<script type="text/javascript">
  /////////////////////////////////////////////////////////////////////
    $('.btndisable').attr('disabled', true);
  $('.one').change(function () {
    //check if checkbox is checked
        if($('input[type=checkbox].one:checked').size() > 0) {    
        $('.btndisable').removeAttr('disabled'); 
        //  $('.sendid').attr('disabled', true);

       //enable input    
    } else {
        $('.btndisable').attr('disabled', true); //disable input

       // $('.sendid').removeAttr('disabled'); 
    }
});
  //end disablesection
////////////////////////////////////////////////////////////////////

   </script>

   

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>    
<!-- ajax edit getdata -->
<script type="text/javascript">
  var $tmp = [];
$('input[type=checkbox].one').change(function(){
$value=$(this).val();
</script>
<!-- ajax edit getdata -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>  
<script type="text/javascript">
$('input[type=checkbox].one').change(function(){
$value=$(this).val();
 
$.ajax({
type : 'get',
url : '../user/search',
data:{'search':$value},
success:function(data){

$('#sendmail').html(data['mail']);
}
});
});
</script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<!-- endscript -->


<style>
  .caption {
    text-align: center;
    font-size: 22px;
    margin: 4px;
  }
    .bs-select {

height: 30px;
width: 100px;
margin: 3px;

}
.modal-backdrop.in {
    opacity: 0;
    
}
.modal{
    width:60%;
}
.modal, .modal-backdrop {
    top: 20%;
}
.btn-circle {
   
    background: #bf2476;
    height: 35px;
    color: #fff;
} 
p {

    color: #0a0a91;
}
.modal-title {
  
    background: #bf2476;
    color: #fff;
    width: 100px;
    text-align: center;
}
.btn.default:not(.btn-outline) {
 
    border-radius: 30px !important;
    background-color: #c9ccd1;
}

#ic {
    color: #bf2476;
}
element {

}
.page-sidebar .page-sidebar-menu > li > a > .title, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu > li > a > .title {

 
    font-size: 16px;
}
.btn-group-lg > .btn, .btn-lg {
  
    font-size: 15px;
}
  </style>


<script language="javascript" type="text/javascript">
    function submitDetailsForm() {
       $("#formId").submit();
    }
</script>



@endsection
