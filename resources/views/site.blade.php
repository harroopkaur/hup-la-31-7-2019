<html>
    <html lang="{{ app()->getLocale() }}">
<head>
<title>Portal husting</title>

  <link rel="stylesheet" href="{{url('assets/landing/style.css')}}">
    <link rel="stylesheet" href="{{url('assets/landing/main.css')}}">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{url('assets/landing/app/styles/progress-tracker.css')}}">
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
       <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet"> 
       <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
<body>
  <div class="overlapsty95">
  <div class="container">
    <div class="row">
<div class="col-md-12 col-sm-12 col-lg-12 main-headersty">
  <div class="col-md-4 wel-comemessage">
<!-- <img class="logo-topsty" src="{{url('assets/landing/img/logo.png')}}"> -->
<p>{{ __('messages.welcome to') }} Hosting Usage Portal</p>
</div>
<div class="col-md-6">
<ul class="top_contlinks">
                        <li><a><!-- <i class="fa fa-phone"></i> --><img class="top-iconsty951" src="{{url('assets/landing/img/colo-phone.png')}}"> (305)851-3545</a></li>
                        <li><a href="mailto:info@licensingassurance.com" title="info@licensingassurance.com"><!-- <i class="fa fa-envelope"></i> --> <img class="top-iconsty951" src="{{url('assets/landing/img/color-email.png')}}"> info@licensingassurance.com</a></li>
                      
            
          
                    </ul>
</div>
<div class="col-md-2 selctlang-sty">

<select class="lang-iconchange" name="forma" onchange="location = this.value;">
<option value="">{{ __('messages.select Language') }}</option>
<option value="{{ url('locale/en') }}">ENGLISH</option>
<option value="{{ url('locale/es') }}">SPANISH</option>
</select>
</div>
</div>
</div>
</div>

<!-- ===============================slider data start================== -->
<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
     
    </div>

    <div class="collapse navbar-collapse center-datamenu pading-zerosty" id="myNavbar">
       <div class="navbar-header">
      <a class="navbar-brand logo-substy" href="{{url('')}}"><img class="logo-topsty whitelogo-show" src="{{url('assets/landing/img/logo-white.png')}}"> <img class="logo-topsty planlogo-show" src="{{url('assets/landing/img/logo.png')}}"></a>
    </div>
      <ul class="nav navbar-nav uldatacenter-menuy951">
        <li><a href="{{url('')}}"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorhome.png')}}">
       {{ __('messages.Menu one') }}
       </a></li>        
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/color-web.png')}}"> 
      {{ __('messages.Menu two') }}
      </a></li>
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/coloridea.png')}}"> {{ __('messages.Menu three') }}
        </a></li>
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorcontact.png')}}"> {{ __('messages.Menu four') }} 
        </a></li>
        <li><a href="#" onclick="openNav()"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorlogin.png')}}"> 
        {{ __('messages.Menu five') }}
      </a></li>
      </ul>
     
    </div>
  </div>
</nav>
  </div>
  <!-- ================================= -->
  <div id="mySidebar" class="sidebar">
<div class="pop-closesty951">
  <span class="closebtn" onclick="closeNav()">×</span>
</div>
<div class="login-maindatahad">
    <p>{{ __('messages.login') }}</p> 
</div>
<div class="main-popdataleft">
  <ul>
      <li><a href="{{url('dcsection/')}}" class="login-s">Log In DC</a></li>
      <li><a href="{{url('user/')}}" class="login-s">Log In End User</a></li>
      <li><a href="{{url('admin/')}}" class="login-s">Log In LA</a></li>
  </ul>
</div>
</div>
<!-- ===================================== -->

  <!-- Full Page Image Background Carousel Header -->
  <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('{{url('assets/landing/img/slider2.jpg')}}');"></div>
                <div class="carousel-caption">
                     <h2 class="animated fadeInLeft">{{ __('messages.endUsers') }}</h2>
                     <p class="animated fadeInUp"> {{ __('messages.access to end user app') }}</p>
                  <!--    <p class="animated fadeInUp"><a href="#" class="btn btn-transparent btn-rounded btn-large">Learn More</a></p> -->
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('{{url('assets/landing/img/slider3.jpg')}}');"></div>
                <div class="carousel-caption">
                     <h2 class="animated fadeInDown">DataCenter</h2>
                     <p class="animated fadeInUp">VM Inventory</p>
                   <!--   <p class="animated fadeInUp"><a href="#" class="btn btn-transparent btn-rounded btn-large">Learn More</a></p> -->
                </div>
            </div>
          
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </div>

<!-- =====================slider data close ======================== -->

<div style="background-color: #fcfcfc;">
<div class="container">
  <div class="row">
    <div class="main-datacenterdata951">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-8 col-xs-12">
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
       <legend class="process-flowsty951"> {{ __('messages.datacenter') }}  </legend>
    </div>
     <div class="col-md-12 col-sm-12 col-xs-12 datacenter-datalist951">
    <p>
      {{ __('messages.indexone') }}
   </p>
    <h5>{{ __('messages.quick look') }} – </h5>
    <ul>
  <li>
    <span class="imgaboutsty951">
      <img class="logo-topsty" src="{{url('assets/landing/img/better1.png')}}">
    </span>
      <h6> {{ __('messages.better efficiency') }} –</h6>
      <p> {{ __('messages.key benefits') }} .</p>
</li>

  <li>
     <span class="imgaboutsty951">
      <img class="logo-topsty" src="{{url('assets/landing/img/better2.png')}}">
    </span>
<h6>{{ __('messages.improved automation') }} –</h6>

<p>{{ __('messages.at Hosting') }} .</p>
</li>

  <li>
     <span class="imgaboutsty951">
      <img class="logo-topsty" src="{{url('assets/landing/img/better3.png')}}">
    </span>
<h6>{{ __('messages.better decisions') }}  –</h6>
<p>{{ __('messages.with Hosting') }} .</p>
</li>

  <li>
     <span class="imgaboutsty951">
      <img class="logo-topsty" src="{{url('assets/landing/img/better4.png')}}">
    </span>
<h6>{{ __('messages.detailed knowledge') }}  –</h6>
<p>{{ __('messages.at Hosting Usage') }} .</p>
</li>
    </ul>
    </div>
  </div>
      </div>
      <div class="col-md-5 col-sm-4 col-xs-12">
        <img class="logo-topsty" src="{{url('assets/landing/img/server-images.png')}}">
        
      </div>
    </div>
  </div>
</div>

  

  </div>
</div>
</div>
<!-- ======================================================= -->
<section class="why_choose">
        <div class="container">
            <div class="row sectionTitle margin-b15">
                <legend class="process-flowsty951"> {{ __('messages.hosting Usage Portal') }}  </legend>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xs-12 cause2choose">
                    <div class="media">
                        <div class="media-left"><a href="#"><img src="{{url('assets/landing/img/speedometer.png')}}"></a></div>
                        <div class="media-body">
                            <h4>{{ __('messages.lowest Latency') }} </h4>
                            <p> {{ __('messages.centers are located') }} .</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 cause2choose">
                    <div class="media">
                        <div class="media-left"><a href="#"><img src="{{url('assets/landing/img/financial.png')}}"></a></div>
                        <div class="media-body">
                            <h4>{{ __('messages.financial Gains') }} </h4>
                            <p>{{ __('messages.host application directly') }} .</p>
                        </div>
                    </div>
                </div>
        <div class="col-sm-6 col-xs-12 cause2choose">
                    <div class="media">
                        <div class="media-left"><a href="#"><img src="{{url('assets/landing/img/support.png')}}"></a></div>
                        <div class="media-body">
                            <h4>{{ __('messages.best Support') }} </h4>
                            <p>{{ __('messages.managing the application') }} .</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 cause2choose">
                    <div class="media">
                        <div class="media-left"><a href="#"><img src="{{url('assets/landing/img/certification.png')}}"></a></div>
                        <div class="media-body">
                            <h4>{{ __('messages.iso Certified') }}</h4>
                            <p>{{ __('messages.infrastructure from') }} .</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 cause2choose">
                    <div class="media">
                        <div class="media-left"><a href="#"><img src="{{url('assets/landing/img/recoveryfile.png')}}"></a></div>
                        <div class="media-body">
                            <h4>{{ __('messages.easy Disaster Recovery') }} </h4>
                            <p>{{ __('messages.requirement of client') }} .</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 cause2choose">
                    <div class="media">
                        <div class="media-left"><a href="#"><img src="{{url('assets/landing/img/connection.png')}}"></a></div>
                        <div class="media-body">
                            <h4>{{ __('messages.better Connectivity') }} </h4>
                            <p>{{ __('messages.avoid any downtime') }} .</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
<!-- =========================================================== -->


<div class="main-stycolor951">
  <!-- ============================================== -->
  <div class="container">
    <div class="row">
<!-- =============== sec part start=============================== -->
<div class="col-md-12 main-databg651">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
       <legend class="process-flowsty951"> {{ __('messages.process Flow') }}  </legend>
    </div>
    <div class="col-md-7 col-sm-12 col-xs-12">
        
      <div class="col-md-12 col-sm-12 col-xs-12 main-martop951">
        <div class="col-md-6 col-sm-12 col-xs-12 end-userimgsty">
          <img src="{{url('assets/landing/img/end-userimg.png')}}">
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="end-userdata951sty">
               <button >{{ __('messages.endUsers') }}</button>
               <h4>{{ __('messages.access to end user app') }}</h4>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 main-martop951">
        <div class="col-md-6 col-sm-12 col-xs-12 end-userimgsty">
          <img src="{{url('assets/landing/img/data-centerimg.png')}}">
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="data-userdata951sty">
               <button >{{ __('messages.datacenter') }}</button>
                <h4>{{ __('messages.vM Inventory') }}</h4>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-5 col-sm-12 col-xs-12 moderan-dataulsty">
      <ul>
       <li>{{ __('messages.modern data center solution') }}</li>
        <li>{{ __('messages.end user automatization information') }} </li>
        <li>{{ __('messages.helping you grow') }} </li>
        <li>{{ __('messages.smart look fast and easy') }}</li>
        <li>{{ __('messages.immediately result') }} </li>
        <!-- <li>{{ __('messages.try it now') }}</li> -->
        <li><div class="arrow bounce">
  <i class="fa fa-arrow-down fa-2x" href="#"></i>
</div></li>
      </ul>
    </div>
</div>
</div> 
<!-- =============== sec part  close =============================== --> 
</div>
</div>


<!-- ================third part start================ -->
<div class="main-bgchange621">
<div class="container">
    <div class="row">
<div class="col-md-12 col-sm-12  main-databg651 main-bgchange621">
  <div class="col-md-12 col-sm-12">
<h3 class="process-flowsty951">{{ __('messages.process Flow') }}</h3>
</div>
<div class="col-md-9 col-sm-9 Ee_vent">
        <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
        <img src="{{url('assets/landing/img/password.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4> 
            </span>
            <span class="progress-marker">1</span>
          </li>
          <li class="progress-step is-complete">
            <span class="progress-text">
          <img src="{{url('assets/landing/img/shelf.png')}}">
              <h4 class="progress-title">{{ __('messages.vM Inventory') }}</h4>          
            </span>
            <span class="progress-marker">2</span>
          </li>
          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('assets/landing/img/add-friend.png')}} ">
              <h4 class="progress-title">{{ __('messages.endUser Management') }}</h4>              
            </span>
            <span class="progress-marker">3</span>
          </li>
          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('assets/landing/img/interview.png')}}">
                 <h4 class="progress-title">{{ __('messages.endUser Request') }}</h4>           
            </span>
            <span class="progress-marker">4</span>
          </li>
          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('assets/landing/img/stats.png')}}">
              <h4 class="progress-title">{{ __('messages.user Inventory') }}</h4>              
            </span>
            <span class="progress-marker">5</span>
          </li>
        </ul>
    </div>
    <div class="col-md-3 col-sm-3 progress_btn">
     <center><h3>{{ __('messages.for Data Center') }}</h3></center>
      <center><a href="{{url('dcsection/')}}"><button  class="login-dcsty">{{ __('messages.log In DC') }}</button></a></center>
    </div>
</div>
</div>
</div>
</div>
<!-- =================third part close=========================== -->
<!-- =====================forth part start=============== -->
<div class="container">
    <div class="row">
<div class="arroeser-pos951">
<img src="{{url('assets/landing/img/arrow.png')}}">
</div>
<div class="col-md-12 col-sm-12  main-databg651">

        <div class="col-md-12 col-sm-12">
<h3 class="process-flowsty951">{{ __('messages.process Flow End user') }}</h3></div>
<div class="col-md-9 col-sm-12 Ee_vent">
     <ul class="progress-tracker progress-tracker--text progress-tracker--text-top procee-flow951">
          <li class="progress-step is-complete">
            <span class="progress-text">
        <img src="{{url('assets/landing/img/password.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4>
             
            </span>
            <span class="progress-marker own_color ">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
          <img src="{{url('assets/landing/img/file.png')}}">
              <h4 class="progress-title">{{ __('messages.assigned License') }}</h4>
          
            </span>
            <span class="progress-marker own_color " >2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('assets/landing/img/literature.png')}}">
              <h4 class="progress-title">{{ __('messages.send to DC update info') }}</h4>
              
            </span>
            <span class="progress-marker own_color " >3</span>
          </li>
        </ul>
  </div>
 <div class="col-md-3 col-sm-12 progress_btn">
      <center><h3>{{ __('messages.for End User') }}</h3></center>
      <center><a href="{{url('user/')}}"><button class="login-enduser951">{{ __('messages.log In End User') }}</button></a></center>
    </div>
</div>
<!-- =======================forth part close=============================== -->
</div>
</div>
<!-- ================================================== -->
</div>

<!-- ============ footer data start ================= -->
<div class="mail-us"><span class="mailusprt1"> {{ __('messages.mail Requirement') }} :</span> <a href="mailto:info@licensingassurance.com"><i class="fa fa-paper-plane"></i>info@licensingassurance.com</a> <span>|</span> <span class="mailusprt2">{{ __('messages.call us') }} :</span> <a href="tel:(305)851-3545"><i class="fa fa-phone"></i>(305)851-3545</a></div>

<footer>
<div class="sub-footer">
<ul>
<li><a href="#">{{ __('messages.dedicated Server Hosting') }} </a></li>
<li><a href="#">{{ __('messages.vPS Hosting') }} </a></li>
<li><a href="#">{{ __('messages.cloud Hosting') }} </a></li>
<li><a href="#">{{ __('messages.server Colocation') }} </a></li>
<li><a href="#">{{ __('messages.data Center') }} </a></li>
<li><a href="#">{{ __('messages.email Server Hosting') }} </a></li>
<li><a href="#">{{ __('messages.application Hosting') }}</a></li>
<li><a href="#">{{ __('messages.domain Registration') }} </a></li>
<li><a href="#">{{ __('messages.sSL Certificates') }} </a></li>
</ul>
</div>
<div class="footer-menu row">
<div class="col-sm-6">
<ul>
<li><a href="#"> {{ __('messages.knowledgebase') }}</a></li>
<li><a href="#"> {{ __('messages.wiki') }} </a></li>
<li><a href="#"> {{ __('messages.forum') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.blog') }} </a></li>
<li><a href="#"> {{ __('messages.resource Library') }} </a></li>
<li><a href="#"> {{ __('messages.clients') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.review') }} </a></li>
<li><a href="#"> {{ __('messages.privacy Policy') }} </a></li>
<li><a href="#"> {{ __('messages.terms Conditions') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.fup Policy') }} </a></li>
<li><a href="#"> {{ __('messages.disclaimer') }} </a></li>
<li><a href="#"> {{ __('messages.xml') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.contact Us') }} </a></li>
<li><a href="#"> {{ __('messages.sitemap') }}</a></li>
</ul>
</div>
<div class="col-sm-6 row">
<div class="col-sm-6 col-xs-12 response">
<strong> {{ __('messages.quick Response') }} </strong>
<a href="info@licensingassurance.com">info@licensingassurance.com</a>
<div class="experts">
<strong> {{ __('messages.speak to Sales Tech Experts') }} </strong>
<a href="(305)851-3545">(305)851-3545</a>
</div>
</div>
<div class="col-sm-6 col-xs-12">
<strong> {{ __('messages.live Chat') }} </strong>
 {{ __('messages.talk to our Technical Sales Representative') }} 
<div class="copy">
© <script>document.write(new Date().getFullYear())</script> Hosting Usage Portal. {{ __('messages.all Rights Reserved') }} .
</div>
</div>
</div>
<!-- <div class="col-sm-12 col-md-12 col-xs-12">
<div style="margin-left:5px; float:left;" class="textfooterimg-right">
<span xmlns:v="http://rdf.data-vocabulary.org/#" typeof="v:Review-aggregate">
<span property="v:itemreviewed" style="color:#666;">Go4Hosting</span>
<span rel="v:rating" style="color:#666;">is rated
<span typeof="v:Rating" style="color:#666;">
<span property="v:average">4.25</span> /
<span property="v:best">5</span>
</span>
</span><span style="color:#666;">in</span>
<span property="v:count" style="color:#666;">45</span> <span style="color:#666;">reviews on</span> <a rel="nofollow" href="https://www.hostreview.com/companies/go4hosting/reviews" style="color:#fff!important;">Host Review</a>.
</span>
</div>
</div> -->
</div>
<div class="payment-bar row">
<div class="col-sm-5 social">
<ul>
<li>{{ __('messages.go Social') }} </li>
<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-telegram"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
</ul>
</div>
<div class="col-sm-6 payment">
<ul>
<li>{{ __('messages.we Accept') }} 
<strong>{{ __('messages.cards cheques and online transfer') }} </strong></li>
<li><img src="{{url('assets/landing/img/maestro.png')}}" width="42" height="26"></li>
<li><img src="{{url('assets/landing/img/master-card.png')}}" width="42" height="26"></li>
<li><img src="{{url('assets/landing/img/footer-icon.png')}}" width="41" height="41"></li>
<li><img src="{{url('assets/landing/img/visa.png')}}" width="50" height="16"></li>
<li><img src="{{url('assets/landing/img/transfer.png')}}" width="37" height="32"></li>
</ul>
</div>
</div>

</footer>
<!-- ============footer data close=============== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{url('assets/landing/app/scripts/site.js')}}"></script>
<script>
$(window).scroll(function(){
    if ($(window).scrollTop() >= 300) {
        $('.navbar-inverse').addClass('fixed-header');
    }
    else {
        $('.navbar-inverse').removeClass('fixed-header');
    }
});
</script>

<script>
function openNav() {
  document.getElementById("mySidebar").style.width = "350px";
  document.getElementById("main").style.marginLeft = "350px";
  document.getElementById("mySidebar").style.marginRight= "10px;";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  document.getElementById("mySidebar").style.marginRight= "-8px;";
}
</script>

</body>
</html>