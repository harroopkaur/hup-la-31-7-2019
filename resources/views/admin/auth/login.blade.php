

@extends('admin.layout.auth')

@section('content')
<html lang="{{ app()->getLocale() }}">
<head>
<title>Portal husting</title>

  <link rel="stylesheet" href="{{url('/assets/dcloginfiles/style.css')}}">
   <link rel="stylesheet" href="{{url('assets/landing/main.css')}}">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
   <link rel="stylesheet" href="{{url('assets/dcloginfiles/app/styles/progress-tracker.css')}}">
   
       <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cutive+Mono|Lato:300,400">
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
<body>
  
<!-- ========================== new header start ================== -->
  <div class="overlapsty95">
  <div class="container">
    <div class="row">
<div class="col-md-12 col-sm-12 col-lg-12 main-headersty">
  <div class="col-md-4 wel-comemessage">
<!-- <img class="logo-topsty" src="{{url('assets/landing/img/logo.png')}}"> -->
<p>{{ __('messages.welcome to') }} Hosting Usage Portal</p>
</div>
<div class="col-md-6">
<ul class="top_contlinks">
                        <li><a><!-- <i class="fa fa-phone"></i> --><img class="top-iconsty951" src="{{url('assets/landing/img/colo-phone.png')}}"> (305)851-3545</a></li>
                        <li><a href="mailto:info@licensingassurance.com" title="info@licensingassurance.com"><!-- <i class="fa fa-envelope"></i> --> <img class="top-iconsty951" src="{{url('assets/landing/img/color-email.png')}}"> info@licensingassurance.com</a></li>
                      
            
          
                    </ul>
</div>
<div class="col-md-2 selctlang-sty">

<select class="lang-iconchange" name="forma" onchange="location = this.value;">
<option value="">{{ __('messages.select Language') }}</option>
<option value="{{ url('locale/en') }}">ENGLISH</option>
<option value="{{ url('locale/es') }}">SPANISH</option>
</select>
</div>
</div>
</div>
</div>

<!-- ===============================slider data start================== -->
<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
     
    </div>

    <div class="collapse navbar-collapse center-datamenu pading-zerosty" id="myNavbar">
       <div class="navbar-header">
      <a class="navbar-brand logo-substy" href="{{url('')}}"><img class="logo-topsty whitelogo-show" src="{{url('assets/landing/img/logo-white.png')}}"> <img class="logo-topsty planlogo-show" src="{{url('assets/landing/img/logo.png')}}"></a>
    </div>
      <ul class="nav navbar-nav uldatacenter-menuy951">
        <li><a href="{{url('')}}"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorhome.png')}}">
       {{ __('messages.Menu one') }}
       </a></li>        
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/color-web.png')}}"> 
      {{ __('messages.Menu two') }}
      </a></li>
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/coloridea.png')}}"> {{ __('messages.Menu three') }}
        </a></li>
        <li><a href="#"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorcontact.png')}}"> {{ __('messages.Menu four') }} 
        </a></li>
        <li><a href="#" onclick="openNav()"> <img class="menu-iconsty" src="{{url('assets/landing/img/colorlogin.png')}}"> 
        {{ __('messages.Menu five') }}
      </a></li>
      </ul>
     
    </div>
  </div>
</nav>
  </div>
<!-- ========================== new header close ================== -->


<!-- ===============================slider data start================== -->

  
  <!-- ================================= -->
  <div id="mySidebar" class="sidebar">
<div class="pop-closesty951">
  <span class="closebtn" onclick="closeNav()">×</span>
</div>
<div class="login-maindatahad">
    <p>{{ __('messages.login') }}</p> 
</div>
<div class="main-popdataleft">
  <ul>
      <li><a href="{{url('dcsection/')}}" class="login-s">Log In DC</a></li>
      <li><a href="{{url('user/')}}" class="login-s">Log In End User</a></li>
      <li><a href="{{url('admin/')}}" class="login-s">Log In LA</a></li>
  </ul>
</div>
</div>
<!-- ===================================== -->

 <div class="main-topbanner951">
  <div class="main-layer951data">
    <div class="container">
      <div class="row">
    <h6>LA Login</h6>
  </div>
  </div>
  </div>
 </div>

<!-- =====================slider data close ======================== -->

<!-- ======================================== -->
<div class="main-stycolor951">
  <div class="container">
    <div class="row">

<!-- =============== sec part start=============================== -->
<div class="col-md-12 main-databg651">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
       <legend class="process-flowsty951">  {{ __('messages.total access') }} </legend>
    </div>
    <div class="col-md-7 col-sm-12 col-xs-12">
        
      <div class="col-md-12 col-sm-12 col-xs-12 main-martop951">
        <div class="col-md-6 col-sm-12 col-xs-12 end-userimgsty">
          <img src="{{url('assets/landing/img/end-userimg.png')}}">
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="end-userdata951sty">
               <button >{{ __('messages.endUsers') }}</button>
               <h4>{{ __('messages.access to end user app') }}</h4>
          </div>
        </div>
      </div>
      <div class="col-md-12 col-sm-12 col-xs-12 main-martop951">
        <div class="col-md-6 col-sm-12 col-xs-12 end-userimgsty">
          <img src="{{url('assets/landing/img/data-centerimg.png')}}">
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
          <div class="data-userdata951sty">
               <button >{{ __('messages.datacenter') }}</button>
                <h4>{{ __('messages.vM Inventory') }}</h4>
          </div>
        </div>
      </div>
    </div>
<!-- ===================== form data start ============= -->
    <div class="col-md-5 col-sm-12 col-xs-12 moderan-dataulsty">
   <div class="form-maindatabg951">

    <div class="form-loginsty951">
 <h6> <!-- {{ __('messages.log In') }} -->  <i class="fa fa-user-plus" aria-hidden="true"></i>
 LogIn LA Portal
 </h6>
</div>

<div class="col-md-12 dcloginn">
  <center>

<form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">
                        {{ csrf_field() }}
     <div class="input-group login-forminputsty">
      <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
     <input id="email" type="email" class="form-control"placeholder="{{ __('messages.email') }}" name="email" value="{{ old('email') }}" autofocus>
    </div>
 @if ($errors->has('email'))
                                    <span class="">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif<br>
    <div class="input-group  login-forminputsty"> 
      <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
     <input id="password" type="password" class="form-control" placeholder="{{ __('messages.password') }}"name="password"> </div>

                                @if ($errors->has('password'))
                                    <span class="">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
<br>

<center><button class="login-dcsty"> {{ __('messages.log In') }}
</button></center>

<div class="create-newacountsty951">
  <a href="#">Create New Account</a>
</div>

</form>
</center>
</div>
<div style="clear: both;"></div>
</div>
    </div>
    <!-- ===================== form data close ============= -->
</div>
</div> 
</div>
</div>
</div>
<!-- =============== sec part  close =============================== --> 
<div style="background-color: #fcfcfc;">
 <div class="container">
    <div class="row">



<div class="col-md-12 col-sm-12  main-databg651">
  <div class="col-md-12 col-sm-12 ">
<h3 class="process-flowsty951">{{ __('messages.process Flow') }} {{ __('messages.datacenter') }}</h3>
</div>
<div class="col-md-12 col-sm-12 te_center ">
        <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
        <img src="{{url('/assets/landing/img/password.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4>

            </span>
            <span class="progress-marker">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
          <img src="{{url('/assets/landing/img/shelf.png')}}">
              <h4 class="progress-title">{{ __('messages.vM Inventory') }}</h4>
          
            </span>
            <span class="progress-marker">2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/landing/img/add-friend.png ')}}">
              <h4 class="progress-title">{{ __('messages.endUser Management') }}</h4>
              
            </span>
            <span class="progress-marker">3</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/landing/img/interview.png')}}">
                 <h4 class="progress-title">{{ __('messages.endUser Request') }}</h4>
           
            </span>
            <span class="progress-marker">4</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/landing/img/stats.png')}}">
              <h4 class="progress-title">{{ __('messages.user Inventory') }}</h4>
              
            </span>
            <span class="progress-marker">5</span>
          </li>
        </ul>
  </div>  
</div>

<div class="arroeser-pos951">
<img src="{{url('assets/landing/img/arrow.png')}}">
</div>
</div>
</div>
</div>


<div class="">
 <div class="container">
    <div class="row">
<div class="col-md-12 col-sm-12 main-databg651">
 <div class="col-md-12 col-sm-12">
      <h3 class="process-flowsty951">{{ __('messages.process Flow End user') }}</h3></div>
      <div class="col-md-12 col-sm-12  te_center">
     <ul class="progress-tracker progress-tracker--text progress-tracker--text-top">
          <li class="progress-step is-complete">
            <span class="progress-text">
        <img src="{{url('/assets/landing/img/password.png')}}">
                  <h4 class="progress-title">{{ __('messages.log In') }}</h4>
             
            </span>
            <span class="progress-marker own_color ">1</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
          <img src="{{url('/assets/landing/img/file.png')}}">
              <h4 class="progress-title">{{ __('messages.assigned License') }}</h4>
          
            </span>
            <span class="progress-marker own_color " >2</span>
          </li>

          <li class="progress-step is-complete">
            <span class="progress-text">
      <img src="{{url('/assets/landing/img/literature.png')}}">
              <h4 class="progress-title">{{ __('messages.send to DC update info') }}</h4>
              
            </span>
            <span class="progress-marker own_color " >3</span>
          </li>
        </ul>
  </div>
</div>




</div>
</div>
</div>
<!-- ======================== new footer  =================== -->

<div class="mail-us"><span class="mailusprt1"> {{ __('messages.mail Requirement') }} :</span> <a href="mailto:info@licensingassurance.com"><i class="fa fa-paper-plane"></i>info@licensingassurance.com</a> <span>|</span> <span class="mailusprt2">{{ __('messages.call us') }} :</span> <a href="tel:(305)851-3545"><i class="fa fa-phone"></i>(305)851-3545</a></div>

<footer>
<div class="sub-footer">
<ul>
<li><a href="#">{{ __('messages.dedicated Server Hosting') }} </a></li>
<li><a href="#">{{ __('messages.vPS Hosting') }} </a></li>
<li><a href="#">{{ __('messages.cloud Hosting') }} </a></li>
<li><a href="#">{{ __('messages.server Colocation') }} </a></li>
<li><a href="#">{{ __('messages.data Center') }} </a></li>
<li><a href="#">{{ __('messages.email Server Hosting') }} </a></li>
<li><a href="#">{{ __('messages.application Hosting') }}</a></li>
<li><a href="#">{{ __('messages.domain Registration') }} </a></li>
<li><a href="#">{{ __('messages.sSL Certificates') }} </a></li>
</ul>
</div>
<div class="footer-menu row">
<div class="col-sm-6">
<ul>
<li><a href="#"> {{ __('messages.knowledgebase') }}</a></li>
<li><a href="#"> {{ __('messages.wiki') }} </a></li>
<li><a href="#"> {{ __('messages.forum') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.blog') }} </a></li>
<li><a href="#"> {{ __('messages.resource Library') }} </a></li>
<li><a href="#"> {{ __('messages.clients') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.review') }} </a></li>
<li><a href="#"> {{ __('messages.privacy Policy') }} </a></li>
<li><a href="#"> {{ __('messages.terms Conditions') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.fup Policy') }} </a></li>
<li><a href="#"> {{ __('messages.disclaimer') }} </a></li>
<li><a href="#"> {{ __('messages.xml') }} </a></li>
</ul>
<ul>
<li><a href="#"> {{ __('messages.contact Us') }} </a></li>
<li><a href="#"> {{ __('messages.sitemap') }}</a></li>
</ul>
</div>
<div class="col-sm-6 row">
<div class="col-sm-6 col-xs-12 response">
<strong> {{ __('messages.quick Response') }} </strong>
<a href="info@licensingassurance.com">info@licensingassurance.com</a>
<div class="experts">
<strong> {{ __('messages.speak to Sales Tech Experts') }} </strong>
<a href="(305)851-3545">(305)851-3545</a>
</div>
</div>
<div class="col-sm-6 col-xs-12">
<strong> {{ __('messages.live Chat') }} </strong>
 {{ __('messages.talk to our Technical Sales Representative') }} 
<div class="copy">
© <script>document.write(new Date().getFullYear())</script> Hosting Usage Portal. {{ __('messages.all Rights Reserved') }} .
</div>
</div>
</div>
<!-- <div class="col-sm-12 col-md-12 col-xs-12">
<div style="margin-left:5px; float:left;" class="textfooterimg-right">
<span xmlns:v="http://rdf.data-vocabulary.org/#" typeof="v:Review-aggregate">
<span property="v:itemreviewed" style="color:#666;">Go4Hosting</span>
<span rel="v:rating" style="color:#666;">is rated
<span typeof="v:Rating" style="color:#666;">
<span property="v:average">4.25</span> /
<span property="v:best">5</span>
</span>
</span><span style="color:#666;">in</span>
<span property="v:count" style="color:#666;">45</span> <span style="color:#666;">reviews on</span> <a rel="nofollow" href="https://www.hostreview.com/companies/go4hosting/reviews" style="color:#fff!important;">Host Review</a>.
</span>
</div>
</div> -->
</div>
<div class="payment-bar row">
<div class="col-sm-5 social">
<ul>
<li>{{ __('messages.go Social') }} </li>
<li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-telegram"></i></a></li>
<li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
</ul>
</div>
<div class="col-sm-6 payment">
<ul>
<li>{{ __('messages.we Accept') }} 
<strong>{{ __('messages.cards cheques and online transfer') }} </strong></li>
<li><img src="{{url('assets/landing/img/maestro.png')}}" width="42" height="26"></li>
<li><img src="{{url('assets/landing/img/master-card.png')}}" width="42" height="26"></li>
<li><img src="{{url('assets/landing/img/footer-icon.png')}}" width="41" height="41"></li>
<li><img src="{{url('assets/landing/img/visa.png')}}" width="50" height="16"></li>
<li><img src="{{url('assets/landing/img/transfer.png')}}" width="37" height="32"></li>
</ul>
</div>
</div>

</footer>
<!-- ========================= new footer close =========================== -->



<script src="{{url('assets/dcloginfiles/app/scripts/site.js')}}"></script>
</body>
</html>

<script>
$(window).scroll(function(){
    if ($(window).scrollTop() >= 300) {
        $('.navbar-inverse').addClass('fixed-header');
    }
    else {
        $('.navbar-inverse').removeClass('fixed-header');
    }
});
</script>

<script>
function openNav() {
  document.getElementById("mySidebar").style.width = "350px";
  document.getElementById("main").style.marginLeft = "350px";
  document.getElementById("mySidebar").style.marginRight= "10px;";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  document.getElementById("mySidebar").style.marginRight= "-8px;";
}
</script>
<style type="text/css">
  .btnbtn{
    color:#fff;
  font-size:16px;
  font-weight:600;
  background:#9f17f6;
  border-radius:10px;
  padding:2% 15% 2% 15%;
  }
</style>
@endsection