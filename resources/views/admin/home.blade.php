@extends('admin.includes.main')

@section('content')  

<div class="col-md-12">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                       <script type="text/javascript">
                           $(document).ready(function(){
                            $('.dt-buttons').hide();
                           });
                       </script>
                                <div class="portlet-body">
                                            <tr class="table table-striped table-responsive">
                                              <span class="hideform">
 <form action="{{url('/admin/action')}}"method="post">
    {{csrf_field()}}
  </span>
                                        <td><a data-toggle="modal" data-target="#modalLoginForm" class="btn grey-salt circle mainhadsty-allbut"> <i class="fas fa-user-plus"></i> {{ __('messages.add DC') }}</a></td>

                                               <td><a data-toggle="modal" data-target="#sss" class="editbtnh btn grey-salt circle btndisable mainhadsty-allbut"> <i class="fas fa-user-edit "></i>{{ __('messages.edit DC') }}</a>
                                               </td>


  <!-- active/inactive -->
<div class="modal fade " id="sssss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content circle">
     
        <h4 class="modal-title w-100 font-weight-bold text-info  actinacheading load-resulthadsty">
          <b> </b></h4>
  
      <div class="modal-body ">
<center><h4 class="bold">Are you sure To <span class="actinacheading"></span> Dc</h4></center><br>
<!-- <center>
<tr><td><input type="checkbox"name="sendemail"value="sendmail"/><span>&nbspNotify Dc by E-mail</span><td></tr></center> -->
  <div class="active-deactivatepopsty">
<span class="actinact">
  </span>

<input data-dismiss="modal" aria-label="Close" type="button" class="btn grey-salt  pl-5 pr-5 add-euclose-butstygray"value="Cancel">
</div>
  </div>
</div></div></div>
  <!-- end inactive -->
   <td><a data-toggle="modal" data-target="#sssss" class="editbtnh btn grey-salt circle btndisable mainhadsty-allbut"> 
    &nbsp<i class="fas fa-user-times"></i>  {{ __('messages.active DC') }} </a></td>

                                                

      <td><a data-toggle="modal" data-target="#mailmodel" class="btn grey-salt circle btndisable mailbtn mainhadsty-allbut"> <i class="fa fa-envelope"></i> {{ __('messages.preview Mail') }}</a></td>

<!-- delete -->
<div class="modal fade " id="ssss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content circle">

        <h4 class="modal-title w-100 font-weight-bold text-info load-resulthadsty"><b>Delete</b></h4>
     
  
      <div class="modal-body ">
<center><h4 class="bold">Are you sure To Delete Dc</h4></center> <br>
<!-- <center>
<input type="checkbox"name="sendemail"value="sendmail"/><span>&nbspNotify Dc by E-mail</span>
</center> -->

<div class="active-deactivatepopsty">
<button  name="delete"type="submit" class="btn grey-salt  btndisable pl-5 pr-5">Yes </button>
<input data-dismiss="modal" aria-label="Close" type="button" class="btn grey-salt  pl-5 pr-5 add-euclose-butstygray"value="Cancel">
</div>
  </div>
</div></div></div>
<!-- end delete -->
    <td><a data-toggle="modal" data-target="#ssss" class="editbtnh btn grey-salt circle btndisable mainhadsty-allbut"> <i class="fas fa-user-edit "></i>
    {{ __('messages.delete DC') }} </a></td>
<td >
    @if(session()->has('success'))
   <span class="portlet box green text-white circle"style="padding:10px!important;color:#fff!important;"> {{session('success')}}  <i class="fa fa-check"></i></span>
    @endif
          </ul>
@if($errors)
      <ul style="list-style-type:none;padding:0px;color:red;background:#fff">
     @foreach ($errors->all() as $error)

                <li style="padding:2px;"><b>{{ $error }}</b></li>
                  
            @endforeach
                </ul>
@endif
</td>
</tr><hr/>
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_2">
<thead>
<tr >

<th><center>{{ __('messages.id') }}</center></th>
 <th><center>{{ __('messages.select') }}</center></th>
<th><center>{{ __('messages.user') }}</center></th>
<th><center>{{ __('messages.password') }}</center></th>
<th><center>{{ __('messages.company') }}</center></th>
<th><center>{{ __('messages.email') }}</center></th>
<th><center>{{ __('messages.status') }}</center></th>

    </tr>
</thead>
    <tbody>
@php
$i1 = 1;
@endphp
        @if($data)

        @foreach($data as $key => $datas)
    <tr>
     
<td><center>{{$i1++}}</center></td>
<td><center><input type="checkbox"class="one" name="action[]"id="toggle" value="{{$datas->id}}"></center>
</td>
<td><center>{{$datas->name}}<center></td>
<td><center>
  <!-- passwoord -->
 
<?php 
for ($i=1; $i <= 6; $i++) { 
 echo 'x';
}
?>
  <center></td>
<td><center>{{$datas->company}}<center></td>
<td><center>{{$datas->email}}<center></td>
  <td><center>@if($datas->status == '1')
<a class="btn btn-sm btn-success disabled">{{ __('messages.active') }}</a>
@else
<a class="btn btn-sm btn-danger disabled">{{ __('messages.inactive') }}</a>
@endif
  <center></td>

    </tr>
@endforeach
@endif
                                        </tbody>
                                    </table>
                                </form>



                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!-- mail model -->
<div class="modal fade " id="mailmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content circle">
     
        <h4 class="modal-title w-100 font-weight-bold text-info load-resulthadsty"><b>{{ __('messages.send Mail') }}</b></h4>

      <div class="modal-body ">
        <!-- mail form -->
 <form method="post" action="{{action('dccontroller@sendmail')}}">

    <div class="row">
      <img src="{{url('/img/mail.png')}}"width="100%">
    </div>
    {{csrf_field()}}
<div class="sendmail"style="margin-bottom:0px">
 


    </div>

      </form>
  </div>
</div></div></div>
                    <!-- end edit model -->
                    <!-- f---------------------------------------------------edit end dc -->







<!-- dcc  edit  model -->
<div class="modal fade " id="sss" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content circle">
      
        <h4 class="modal-title w-100 font-weight-bold text-info load-resulthadsty"><b>{{ __('messages.edit DC') }}</b></h4>
     
    
      <div class="modal-body ">
        <!-- dc form -->
 <form method="post" action="{{url('/admin/update')}}">
{{csrf_field()}}
<div class="editdcform"style="margin-bottom:8px"id="editdcform">

    </div>
      </form> 
  </div>
</div></div></div>
<!-- end dc edit model -->


                    <!-- model dc form -->
                    <style type="text/css">
                        .sendmail{
  text-decoration:none!important;
}
                    </style>
                    <!--add dc model  -->
<div class="modal fade " id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content circle">
     
        <h4 class="modal-title w-100 font-weight-bold text-info load-resulthadsty"><b>{{ __('messages.add DC') }}</b></h4>
     
    
      <div class="modal-body ">
        <!-- dc form -->
 <form method="post" action="{{url('/admin/save')}}">
{{csrf_field()}}
<div class="input-group input-groupmarsty">
<span class="input-group-addon">
<i class="fa fa-user"></i>
</span>
<input type="text" class="form-control" placeholder="{{ __('messages.enter username') }}"name="name"> </div>
   
   <div class="input-group input-groupmarsty">
<span class="input-group-addon">
<i class="fa fa-lock fa-fw"></i>
</span>
<input type="password" class="form-control" placeholder="{{ __('messages.enter password') }}"name="password"> </div>  
<div class="input-group input-groupmarsty">
<span class="input-group-addon">
<i class="icon-calendar"></i>
</span>
<input type="text" class="form-control" placeholder="{{ __('messages.company name') }}"name="company"> </div>
<div class="input-group input-groupmarsty">
<span class="input-group-addon">
<i class="fa fa-envelope"></i>
</span>
<input type="email" class="form-control" placeholder="{{ __('messages.email address') }}"name="email"> </div>

<input type="checkbox"name="sendemail"value="sendmail"/><span>&nbspNotify Dc by E-mail</span>

   <div class="pop-addeusty">
<input type="submit" class="btn btn-info circle" value="{{ __('messages.submit') }}"> 

<input data-dismiss="modal" aria-label="Close" type="button" class="btn grey-salt circle close-butstygray"value="{{ __('messages.cancel') }}"> 


    </form>
<!-- end dc form -->
    </div>
</div>
  </div>
</div>


@endsection 