@extends('admin.includes.main')
@section('content')
@php
$i = 1
@endphp

<style>
.pagination {
    
    float: right;
}
</style>
<!-- BEGIN PAGE HEADER-->
<!-- <h1 class="page-title">Resources 
    <small>List of Resources</small>
</h1>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li class="back-btn">
			<a href="javascript: history.go(-1)">Back</a>
			<i class="fa fa-angle-right"></i>
		</li>
        <li>
            <a href="{{ url('admin/resources', [$id]) }}">Resources</a>
        </li>

    </ul>
</div> -->
<!-- END PAGE HEADER-->


@if (session('success'))
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif
@if (session('d_success'))
<div class="alert alert-success">
    {{ session('d_success') }}
</div>
@endif

<!-- BEGIN EXAMPLE TABLE PORTLET-->
<form action=" {{ url('admin/delete')}}" method="post">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-12">
        <div class="btn-group">
             <a href="{{ route('admin.sources', [$id]) }}"><span id="sample_editable_1_new" class="btn green">Add New
                    Resources <i class="fa fa-plus"></i></span> 

            </a>
        </div>
        
        <div class="btn-group">
             <button type="submit" id="sample_editable_1_new" class="btn red delete">Delete
                    Resources <i class="fa fa-trash-o"></i></button> 

           
        </div>
    </div>
</div><br>

<!-- <div class="portlet box green">
<div class="portlet-title">
    <h2 class="font-sizemainhad986">
      <i class="fa fa-globe"></i>Category:@foreach($cat as $result)
    {{$result->name}}
    @endforeach </h2>
      <div class="tools"> 

      </div>
    </div>
</div> -->
 <h2 class="font-sizemainhad986">
      Category:@foreach($cat as $result)
    {{$result->name}}
    @endforeach </h2>
<div class="table-toolbar">
            <div class="form-group">
               <!--  <label class="col-md-1 control-label">Search</label> -->
                <div class="col-md-4">
                <input class="cat-searchsty954" id="searchbar" onkeyup="search_animal()" type="text"
        name="search" placeholder="Search .."> 
                </div>
            </div>
        </div>
        
<div class="row profile-account">
    <div class="col-md-3">
        <ul class="ver-inline-menu tabbable margin-bottom-10 cat-submenutabs95">
            <li class="active">
                <a data-toggle="tab" href="#tab_1-1">
                    <i class="fa fa-file-picture-o" style="color:#17C4BB"></i> Image </a>
                <span class="after"> </span>
            </li>
            <li>
                <a data-toggle="tab" href="#tab_2-2">
                    <i class="fa fa-file-video-o" style="color:#17C4BB"></i> Video </a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab_3-3">
                    <i class="fa fa-file-pdf-o" style="color:#17C4BB"></i> PDF </a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab_4-4">
                    <i class="fa fa-file" style="color:#17C4BB"></i> DOC </a>
            </li>
        </ul>
    </div>
    <div class="col-md-9 catgory-contminsty">
        <div class="tab-content">
            <div id="tab_1-1" class="tab-pane active">
            
                <div class="col-md-12 " >
                    @foreach($image as $value)
                    <div class="col-md-2  gal-item animals" >
                    
                        <div class="box">
                        <img class="img-responsive gallery_img" id="myImg"
                                src="{{asset('img/'.$value->image.'')}}" alt="{{$result->title}}" width="100"
                                height="100">
                            <div id="myModal" class="modal">
                                <span class="close">X</span>
                                <img class="modal-content" id="img01">
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="heading text-center" >
                        
                                   <span class="text-center">{{$value->title}}</span>        
                        </div> 
                        <span class="text-center"> 
                            <input type="checkbox" class="ckeckbox" name="check[]" value="{{$value->id}}">Select</form></span>                 
                    </div>
                    @endforeach
                </div>
                {{$image->render()}}
            </div>

            <div id="tab_2-2" class="tab-pane">
                <div class="col-md-12">
                    @foreach($video as $value)
                    <div class="col-md-2  gal-item animals"  >
                   
                        <div class="box text-center">
                            <a href="{{asset('public/img/'.$value->image.'')}}" download>
                                <video controls class="video">
                                    <source src="{{asset('public/img/'.$value->image.'')}}" type="video/mp4">
                                    <source src="{{asset('public/img/'.$value->image.'')}}" type="video/ogg">

                                </video>
                            </a>

                        </div>
                        <div class="heading text-center"  >
                        
                        {{$value->title}}
                                      
                        </div>
                        <span class="text-center">
                            <input type="checkbox" class="ckeckbox" name="check[]" value="{{$value->id}}">Select</span>
                    </div>

                    @endforeach
                </div>
                {{$video->render()}}
            </div>

            <div id="tab_3-3" class="tab-pane">
                <div class="col-md-12">
                    @foreach($pdf as $value)
                    <div class="col-md-2  gal-item animals " >
                    
                        <div class="box">
                        <img class="img-responsive gallery_img" id="myImg"
                                src="{{asset('public/img/'.$value->image.'')}}" alt="{{$result->title}}" width="100"
                                height="100">
                            <div id="myModal" class="modal">
                                <span class="close">X</span>
                                <img class="modal-content" id="img01">
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="heading text-center" >
                        
                       {{$value->title}}
                        </div>
                        <span class="text-center">
                            <input type="checkbox" class="ckeckbox" name="check[]" value="{{$value->id}}">Select</span>
                    </div>

                    @endforeach
                </div>
                {{$pdf->render()}}
            </div>
            <div id="tab_4-4" class="tab-pane">
                <div class="col-md-12">
                    @foreach($doc as $value)
                    <div class="col-md-2  gal-item animals " >
                    
                        <div class="box">
                        <img class="img-responsive gallery_img" id="myImg"
                                src="{{asset('public/img/'.$value->image.'')}}" alt="{{$result->title}}" width="100"
                                height="100">
                            <div id="myModal" class="modal">
                                <span class="close">X</span>
                                <img class="modal-content" id="img01">
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="heading text-center" id="myUL">
                            {{$value->title}}</span>              
                        </div>
                        <span class="text-center"><input type="checkbox" class="ckeckbox" name="check[]" value="{{$value->id}}">Select</span>
                    </div>

                    @endforeach
                </div>
                {{$doc->render()}}
            </div>
        </div>
    </div>
    <!--end col-md-9-->

</form>

@endsection