@extends('admin.includes.main')
@section('content')
@php
$i = 1
@endphp


<!-- BEGIN PAGE HEADER-->
<h1 class="page-title">Resources Dashboard
  <small>List of Category</small>
</h1>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="icon-home"></i>
      <a href="">Home</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li class="back-btn">
			<a href="javascript: history.go(-1)">Back</a>
			<i class="fa fa-angle-right"></i>
		</li>
    <li>
     <a >Category</a>
   </li>
  
</ul>
</div>
<!-- END PAGE HEADER-->


@if (session('success'))
<div class="alert alert-success">
  {{ session('success') }}
</div>
@endif
@if (session('d_success'))
<div class="alert alert-success">
  {{ session('d_success') }}
</div>
@endif

<!-- BEGIN EXAMPLE TABLE PORTLET-->
 <div class="row">
  <div class="col-md-12">
    <div class="btn-group">
      <a href="{{ url('admin/addresources') }}"><button id="sample_editable_1_new" class="btn green">Add New Resources <i class="fa fa-plus"></i></button>

      </a>
    </div>
  </div>
</div><br> 

<div class="portlet box green">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-globe"></i>List of Category </div>
      <div class="tools"> 

      </div>
    </div>
    <div class="portlet-body">
      <div class="table-toolbar">
        <div class="form-group">
          <label class="col-md-1 control-label"></label>
          <div class="col-md-2">
            
          </div>
        </div>
      </div>
      <table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
            <th> ID </th>
            <th> Name </th>
            <th> Description </th>
            
            <th> Action </th>
          </tr>
        </thead>
        <tbody>
          @foreach($category as $result) 
          <tr>
            <td>{{  $i++ }}</td>
            <td>{{$result->name}}</td>
            <td>{{$result->description}}</td>
            <td>
              <a href="{{ url('admin/resources', [$result->id]) }}" class="btn btn-sm green">
                View Resources<i class="fa fa-eye"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  @endsection