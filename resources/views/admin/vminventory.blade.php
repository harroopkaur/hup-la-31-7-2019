@extends('admin.includes.main')

@section('content')
<style type="text/css">
  .dcdc{
    background:#5a1187!important ;
    color:#fff;
    font-weight:600!important;
    font-size:20px!important;
    text-align:center!important;
  }
    .eueu{
    background:#a30082!important ;
    color:#fff;
      font-weight:600!important;
    font-size:20px!important;
    text-align:center!important;
  }
.valign{
  vertical-align:middle!important;
}
  td{
    padding:4px!important;
  }
    th{
    padding:4px!important;
  }
</style>
<div class="col-md-12">
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                       <script type="text/javascript">
                           $(document).ready(function(){
                            $('.dt-buttons').hide();
                           });
                       </script>

                           <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="container">
                               @if(session()->has('success'))
   <span class="portlet box green text-white circle"style="padding:10px!important;color:#fff!important;"> {{session('success')}}  <i class="fa fa-check"></i></span>
    @endif
    @if($errors)
      <ul style="list-style-type:none;padding:0px;color:red;background:#fff">
     @foreach ($errors->all() as $error)

                <li style="padding:2px;"><b>{{ $error }}</b></li>
                  
                   
            @endforeach
                </ul>
@endif
                            </div>
                          
                        </div>
                            <!-- END EXAMPLE TABLE PORTLET-->


<div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                               <div class="portlet-body">
                                
                                    <table class="table table-striped table-bordered table-hover BBBBB "style="" id="sample_1">


                                        <thead>
                                               <tr>
  <td colspan="9" class="dcdc">{{ __('messages.datacenter') }}</td>
   <td colspan="10"class="eueu">{{ __('messages.endUsers') }}</td>
</tr>
                                       
                                         <tr class="table-imgtop">

                                                <th class="valign">{{ __('messages.id') }}</th>
                                                
                                                <th class="text-center valign"> End&nbspUser
                                                </th>
                                                <th class="text-center valign"> {{ __('messages.update data') }}</th>
                                                <th class="text-center  valign"> {{ __('messages.vm') }}</th>
                                                 <th class="text-center valign"> {{ __('messages.host') }}</th>
                                                <th class="text-center valign"> {{ __('messages.cores') }} </th>
                                                <th class="text-center valign"> {{ __('messages.procs') }}</th>
                                                <th class="text-center valign"> {{ __('messages.date install') }}</th>
                                                 <th class="text-center valign"> 
  <img  src="{{url('/assets/layouts/layout2/img/sql.png')}}" alt="sql"/><br>
                           {{ __('messages.sql') }}
                        </th>
                                                <th class="text-center valign"> 
 <img  src="{{url('/assets/layouts/layout2/img/office.png')}}" alt="office"/><br>
                                                {{ __('messages.office') }}</th>
                                                <th class="text-center valign">
 <img  src="{{url('/assets/layouts/layout2/img/sharepoint.png')}}" alt="logo"/><br>
                                                 {{ __('messages.sharepoint') }}</th>
                                                <th class="text-center valign">
 <img  src="{{url('/assets/layouts/layout2/img/exchange.png')}}"alt="logo"/><br>
                                                 {{ __('messages.exchange') }} </th>
                                                <th class="text-center valign">
 <img  src="{{url('/assets/layouts/layout2/img/skype.png')}}"alt="logo"/><br>
                                                 {{ __('messages.skypebiz') }}</th>
                                                <th class="text-center valign"> 
 <img  src="{{url('/assets/layouts/layout2/img/visualstudio.png')}}"alt="logo"/><br>
                                                {{ __('messages.visual Studio') }}</th>
                                                <th class="text-center valign">
 <img  src="{{url('/assets/layouts/layout2/img/dynamic.png')}}" alt="logo"/><br>
                                                {{ __('messages.dynamic') }}</th>
                                                <th class="text-center valign"> 
 <img  src="{{url('/assets/layouts/layout2/img/project.png')}}" alt="logo"/><br>
                                               {{ __('messages.project') }}</th>
                                                <th class="text-center valign">
 <img  src="{{url('/assets/layouts/layout2/img/rds.png')}}" alt="logo"/><br>
                                                 {{ __('messages.rds') }} </th>
                                                <th class="text-center valign">
 <img  src="{{url('/assets/layouts/layout2/img/vision.png')}}" alt="logo"/><br>
                                                {{ __('messages.visio') }}</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                          $i = 1;
                                          @endphp
                                                  @foreach($vmdata as $key => $udata)  
                                            <tr>
                                          @php
$name=explode(',',$udata->name);

                                          @endphp
                          <td class="text-center valign">{{$i++}}</td>
                        
                          <td class="text-center valign"rowspan=""> {{$name[0]}}</td>
                          <td class="text-center valign">{{date('d-M-Y',strtotime($udata->updatedate))}}</td>
                     <td class="text-center valign">{{$udata->vm}}</td>
                          <td class="text-center valign">{{$udata->host}}</td>
                          <td class="text-center valign">{{$udata->cores}}</td>
                          <td class="text-center valign">{{$udata->procs}}</td>
                          <td class="text-center valign">{{date('d-M-Y',strtotime($udata->dateinstall))}}</td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->sql) {{$udata->sql}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->office) {{$udata->office}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->sharepoint) {{$udata->sharepoint}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->exchange) {{$udata->exchange}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->skypebiz) {{$udata->skypebiz}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->visualstudio) {{$udata->visualstudio}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->dynamics) {{$udata->dynamics}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->project) {{$udata->project}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->rds) {{$udata->rds}} @else Select @endif</option></select></td>
                          <td class="text-center valign"><select class="btn disabled"><option>@if($udata->visio) {{$udata->visio}} @else Select @endif</option></select></td>
                 
                        
                                                
                                            </tr>
                                            @endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                          </div>



@endsection