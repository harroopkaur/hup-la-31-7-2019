@extends('dcsection.includes.main')

@section('content')

<style>
	.bg{
		background:#fff!important;
	}
</style>
<div>
	<div class="col-md-12 bg" >
	<h2 class="font-sizemainhad986"> <b> {{ __('messages.data Center Template For End User') }}  </b></h2>
		<table class="table text-center table-striped table-bordered table-hover table-condensed table-centertem658">
			<tr class="template-data954user">
				<th class="text-center">{{ __('messages.id') }} </th>
				<th class="text-center">{{ __('messages.name') }}</th>
				<th class="text-center">{{ __('messages.action') }}</th>

			</tr>
			@php
			$sr = 1;
			@endphp
			@foreach($tmps as $tmp)
			<tr>
				<td>{{$sr++}}</td>
				<td>{{$tmp->name}}</td>
				<td><a href="{{url('dcsection/edittmp',$tmp->id)}}"class="btn purple">{{ __('messages.edit Template') }}</a></td>

			</tr>
			@endforeach
		</table>

	</div>
</div>

@endsection