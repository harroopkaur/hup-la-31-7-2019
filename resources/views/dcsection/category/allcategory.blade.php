@extends('dcsection.includes.main')
@section('content')
@php
$i = 1
@endphp


<!-- BEGIN PAGE HEADER-->
<!-- <h1 class="page-title">Category 
  <small>List of Category</small>
</h1>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="icon-home"></i>
      <a href="">Home</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li class="back-btn">
			<a href="javascript: history.go(-1)">Back</a>
			<i class="fa fa-angle-right"></i>
		</li>
    <li>
     <a>Category</a>
   </li>
  
</ul>
</div> -->



@if (session('success'))
<div class="alert alert-success">
  {{ session('success') }}
</div>
@endif
@if (session('d_success'))
<div class="alert alert-success">
  {{ session('d_success') }}
</div>
@endif

<!-- BEGIN EXAMPLE TABLE PORTLET-->
 <!--<div class="row">
  <div class="col-md-12">
    <div class="btn-group">
      <a href="{{ url('dcsection/addcategory') }}"><button id="sample_editable_1_new" class="btn green">Add New Category <i class="fa fa-plus"></i></button>

      </a>
    </div>
  </div>
</div><br>--> 

<div class="portlet box white" style="padding-top: 5px !important;">
  <!-- <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-globe"></i>List of Category </div>
      <div class="tools"> 

      </div>
    </div> -->
    <h2 class="font-sizemainhad986"> <b>  <th> {{ __('messages.list of Category') }} </th> </b></h2>
    <div class="portlet-body">
     <!--  <div class="table-toolbar">
        <div class="form-group">
          <label class="col-md-1 control-label"></label>
          <div class="col-md-2">
            
          </div>
        </div>
      </div> -->
      <table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr class="template-data954user center-alldatasty">
            <th> {{ __('messages.id') }} </th>
            <th> {{ __('messages.name') }} </th>
            <th> {{ __('messages.description') }}Description </th>            
            <th> {{ __('messages.action') }} </th>
          </tr>
        </thead>
        <tbody>
          @foreach($category as $result) 
          <tr>
            <td>{{  $i++ }}</td>
            <td>{{$result->name}}</td>
            <td>{{$result->description}}</td>
            <td>
            
              <a href="{{ route('dcsection.resources', [$result->id]) }}" class="btn btn-sm purple">
              {{ __('messages.resources') }} 
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  @endsection