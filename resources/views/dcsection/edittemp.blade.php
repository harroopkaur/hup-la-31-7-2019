@extends('dcsection.includes.main')

@section('content') 


 <script type="text/javascript" src="{{url('/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{url('/ckfinder/ckfinder.js')}}"></script>
<div>
    <h2 class="font-sizemainhad986">Update  {{Ucfirst($tmps[0]->name)}} Template</h2>
<form action="{{url('dcsection/tmpupdate')}}"method="post">
	{{csrf_field()}}

    @if($tmps)
	<input class="form-control"placeholder="{{ __('messages.enter Template name') }}"type="hidden" name="name"value=" {{$tmps[0]->name}}"required><br>
    @else 
    <input class="form-control"placeholder="{{ __('messages.enter Template name') }}"type="hidden" name="name"required><br>

     @endif
     @php

     @endphp
     <input type="hidden" name="idt"value="{{$tmps[0]->id}} " required>
	   <textarea  id="editor1" name="tmp" rows="10" cols="80" required> @if($tmps) {{$tmps[0]->tmp}}@else &nbsp @endif</textarea>

    </p>
 
<script type="text/javascript">
var editor = CKEDITOR.replace( 'editor1', {
    filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
});
CKFinder.setupCKEditor( editor, '../' );
</script>
	<input type="submit" name="submit"value="{{ __('messages.update') }}"class="btn btn-info">
</form>







</div>






@endsection