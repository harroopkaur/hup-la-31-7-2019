
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu sidebatsty-change951" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200"><br>
                        <li class="nav-item start ">
                            <a href="{{url('/dcsection/home')}}" class="nav-link nav-toggle">
                                <!-- <i class="icon-home"></i> -->
                                <img  src="{{url('/assets/layouts/layout2/img/home.png')}}">
                                <span class="title">{{ __('messages.vM Inventory') }}</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>
                        <li class="nav-item start ">
                            <a href="{{url('/dcsection/EnduserManagement')}}" class="nav-link nav-toggle">
                               <!--  <i class="icon-briefcase"></i> -->
                                <img  src="{{url('/assets/layouts/layout2/img/suitcase.png')}}">
                        <span class="title">{{ __('messages.end User Management System') }}</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>
                        <li class="nav-item start ">
                            <a href="{{url('/dcsection/tmpmanage')}}" class="nav-link nav-toggle">
                                <!-- <i class="icon-settings"></i> -->
                                <img  src="{{url('/assets/layouts/layout2/img/settings.png')}}">
                                <span class="title">DC Template Manage</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>
                         <li class="nav-item start nobordersty">
                            <a href="{{url('/dcsection/category')}}" class="nav-link nav-toggle">
                               <!--  <i class="icon-notebook"></i> -->
                               <img  src="{{url('/assets/layouts/layout2/img/notebook.png')}}">
                                <span class="title">{{ __('messages.user Guide') }}</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>
                       <ul>
         
   <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
           
            <!-- END SIDEBAR -->
           
            <!-- BEGIN CONTENT -->
