<?php
return [
    'welcome'       => 'Bienvenido a su aplicación',
   'process Flow' => 'Flujo de proceso',
     'endUsers'   =>   'Usuario Final',
       'datacenter'  => 'Data Center',
       'access to end user app'  =>  'Acceso a la aplicación de usuario final',
       'vM Inventory' => 'Inventario de máquinas virtuales',
       'modern data center solution' => 'Moderna solución de centro de datos',
       'end user automatization information' => 'Información de automatización del usuario final',
       'helping you grow' => 'Ayudándote a crecer',
       'smart look fast and easy' => 'Apariencia inteligente rápida y fácil',
       'immediately result' => 'Resultado inmediato',
       'try it now' => 'Pruebalo ahora',
       'log In' => 'iniciar sesión',
       'endUser Management' => 'Gestión de usuario final',
       'endUser Request' => 'Solicitud de usuario final',
       'user Inventory' => 'Inventario de usuario',
       'for Data Center' => 'for Data Center',
       'log In DC' => 'Inicia sesión DC',
       'process Flow End user' => 'Usuario final del flujo de proceso',
       'assigned License' => 'Licencia Asignada',
       'send to DC update info' => 'Enviar a DC información actualizada',
       'for End User' => 'Para usuario final',
       'log In End User' => 'Iniciar sesión usuario final',
       'send to DC update' => 'Enviar Actualización a DC',
       'user' => 'Usuario',
       'password' => 'Contraseña',
       'forgot your password?' => 'Olvidó su Contraseña?',
       'login' => 'Entrar',
       'manage your Information in Seconds' => 'Administra tu Información en Segundos' ,
       'track expiration of Contracts and Notifications' => 'Ratrea vencimiento de Contratosy Notificaciones',
       'see reports and graphics of your VMs' => 'Ver reportes y graficos de tus VMs',
       'send your update to your provider' => 'Envia a tu porveedor tu actualizacion',
       'logout' => 'cerrar sesión',
       'refresh info' => 'Actualizar informacion',
       'sent info' => 'información enviada',
       'update data' => 'Actualizar Datos',
       'cores' => 'Cores',
       'vm' => 'VM',
       'procs' => 'Procs',
       'date install' => 'Fecha de instalación',
       'sql' => 'SQL',
       'office' => 'Oficina',
       'sharepoint' => 'Usuario Final',
       'exchange' => 'Intercambiar',
       'skypebiz' => 'SkypeBiz',
       'visual Studio' => 'Estudio visual',
       'dynamic' => 'Dinámica',
       'project' => 'Proyecto',
       'rds' => 'RDS',
       'visio' => 'Visio',
       'total access' => 'Acceso total',
       'username' => 'nombre de usuario',
       'create new account' => 'Crear una nueva cuenta',
       'enter email' => 'Ingrese correo electrónico',
       'enter password' => 'Introducir la contraseña',
       'sr.No.' => 'No Señor.',
       'action' => 'Acción',
       'setting' => 'Ajuste',
       'host'=> 'Host',
       'add DC' =>'Añadir DC',
       'edit DC' => 'Editar DC',
       'inactive DC' => 'DC inactivo',
       'preview Mail' => 'Vista previa del correo',
       'delete DC'=> 'Eliminar dc',
       'id'=> 'CARNÉ DE IDENTIDAD',
      'active' => 'Activo',
      'inactive' => 'Inactivo',
       'active DC' => 'Incativar/Activar DC',
       'select' => 'Seleccionar',
       'company' => 'Empresa',
       'email' => 'Correo Electrónico',
       'status' =>  'Estado',
       'end User Management System' =>  'Sistema de gestión de usuario final',
       'download VTool' => 'Descargar VTool',
       'load Result' => 'Resultado de carga',
       'user Guide' => 'Guía del usuario',
       'refresh' => 'Refrescar',
       'add Host' => 'agregar Host',
       'add VM' => 'agregar VM',
       'name' => 'nombre',
       'add VM Form' => 'Añadir formulario VM',
       'add New VM' => 'Añadir nueva máquina virtual',
       'edit VM' => 'Editar VM',
       'edit VM Form' => 'Editar formulario VM',
       'add EU' => 'Añadir UE',
       'edit EU' => 'Editar UE',
       'inactive EU' => 'UE inactiva',
       ' preview Mail' => 'Vista previa del correo',
       'delete DU' => 'Eliminar DU',
       'type Contract' => 'Tipo de contrato',
       'date Contract' => 'Fecha de contrato',
       'comment' => 'comentario',
       'send Mail' => 'enviar correo',
       'preview email' => 'Vista previa de correo electrónico',
       'update' => 'actualizar',
       'assign End User' => 'Asignar usuario final',
       'enter username' => 'introduzca su nombre de usuario',
       'email address' => 'dirección de correo electrónico',
       'enter type contract' => 'entrar tipo contrato',
       'dd/mm/yyyy' => 'dd/ mm/aaaa',
       'submit' => 'enviar',
       'cancel' => 'cancelar',
       'you are Sure to download VTool' => 'Usted está seguro de descargar VTool',
       'upload Result' => 'subir el resultado',
       'enter company name' => 'Ingrese el nombre de la compañía',
       'messages' => 'mensajes',
       'write Your Reply' => 'Escribe tu respuesta',
       'send' => 'enviar',
      'write your Reply here!'  => 'Escriba su respuesta aquí!',
      'manage Mail Template' => 'Administrar Plantillas DC',
      'template List' => 'Lista de plantillas',
      'edit Template' =>'Editar plantilla',
      'create Template' => 'Crear plantilla',
      'enter Template name' => 'Ingrese el nombre de la plantilla',
      'add New Category' => 'Añadir nueva categoria',
      'list of Category' => 'Lista de categoria',
      'description' => 'Descripción',
      'edit' => 'Editar',
      'delete' => 'Borrar',
    'resources' => 'Recursos',
    'add Category' => 'Agregar categoria',
    'content' => 'Contenido',
    'data Center Template For End User' => 'Plantilla de centro de datos para usuario final',
    'indexone' => 'Hosting Usage Portal es uno de los nombres principales cuando se trata de servicios en la nube o infraestructura de centros de datos. Teniendo en cuenta las diferentes necesidades de la industria, Hosting Usage Portal ofrece servicios de administración de infraestructura de centro de datos de extremo a extremo con una inspección detallada de todos los problemas. Los servicios de DCIM del portal de uso de alojamiento ofrecen varios beneficios notables, como productividad mejorada, mejor retorno de la inversión y ahorro de costos y tiempo. ',
    'quick look' => 'Rápido vistazo a los beneficios',
    'better efficiency' => 'Mejor eficiencia y una mejorada disponibilidad',
    'key benefits' => 'Uno de los beneficios clave de los servicios DCIM del portal de uso de hosting es que le ayudamos a recopilar toda la información organizativa de una manera organizada',
    'improved automation' => 'Automatización mejorada y menor riesgo',
    'at Hosting' => 'En Hosting Usage Portal creemos en la teoría de la automatización y sugerimos a nuestros clientes que sigan la regla de automatización donde sea aplicable',
    'better decisions' => 'Mejores decisiones',
    'with Hosting' => 'Con el servicio de infraestructura del centro de datos administrado de Hosting Usage Portal, puede saber qué es exactamente lo que tiene y cómo puede usarlo para mejorar su negocio',
    'detailed knowledge' => 'Conocimiento detallado de la infraestructura',
    'at Hosting Usage' => 'En Hosting Usage Portal, le damos una gran importancia al conocimiento. Conocer los componentes de almacenamiento y redes, así como qué VM se coloca en qué servidor físico le ayudará a controlar mejor las cosas',
    'hosting Usage Portal' => 'Hosting Usage Portal Data Center Advantages',
    'lowest Latency' => 'Latencia más baja',
    'centers are located' => 'Los centros de datos de Using Hosting Portal están ubicados en Calcuta. Esto nos convierte en la primera opción para los clientes de centros de datos de la India, ya que están garantizados con la latencia más baja',
    'financial Gains' => 'Ganancias financieras',
    'host application directly' => 'El alojamiento de servicios de centro de datos con nosotros ayudará a reducir el presupuesto general de TI. Uno puede alojar la aplicación directamente en el centro de datos de Hosting Usage Portal y nuestro equipo se ocupará de los mejores servicios y tiempo de actividad',
    'best Support' => 'Mejor soporte',
    'managing the application' => 'Nuestros centros de datos brindan servicios gestionados al cliente. Estos servicios incluyen la administración de la aplicación, la actualización de parches y la instalación del sistema operativo. Brindamos soporte 24X7 a nuestros clientes',
    'iso Certified' => 'Certificada ISO',
    'infrastructure from' => 'Los centros de datos del Portal cuentan con infraestructura certificada ISO 27001: 2013 de SGS. Esto garantiza que los centros de datos de Hosting Usage Portal sean altamente seguros y confiables',
    'easy Disaster Recovery' => 'Facil recuperacion de desastres',
    'requirement of client' => 'Tenemos dos centros de datos en la India, ubicados en Calcuta respectivamente. Estos centros de datos se encuentran en diferentes zonas sísmicas. Según el requisito del cliente, podemos proporcionar fácilmente un sitio de DR',
    'better Connectivity' => 'Mejor conectividad',
    'avoid any downtime' => 'En nuestros centros de datos, hemos integrado todo el ISP tier1 para proporcionar el mejor tiempo de actividad. Estos ISP están en enrutamiento BGP para evitar cualquier tiempo de inactividad',
    'welcome to' => 'Bienvenido a',
    'select Language' => 'Seleccione lenguaje',
    'Menu one' => 'Inicio',
    'Menu two' => 'Data Center',
    'Menu three' => 'Base de conocimientos',
    'Menu four' => 'Contacto',
    'Menu five' => 'Iniciar sesion',
    'mail Requirement' => 'Envienos su requerimiento',
    'call us' => 'Llamenos',
    'knowledgebase' => 'Base de',
    'wiki' => 'Wiki',
    'forum' => 'Foro',
    'blog' => 'Blog',
    'resource Library' => 'libreria de recursos',
    'clients' => 'Clientes',
    'review' => 'Review',
    'privacy Policy' => 'Politica de privacidad',
    'terms Conditions' => 'Terminos y condiciones',
    'fup Policy' => 'Politica FUP',
    'disclaimer' => 'Disclaimer',
    'xml' => 'XML',
    'contact Us' => 'Cotactenos',
    'sitemap' => 'Mapa de sitio',
    'quick Response' => 'Respuesta rapida',
    'speak to Sales Tech Experts' => 'Hable con experto en ventas',
    'live Chat' => 'Chat en vivo',
    'talk to our Technical Sales Representative' => 'Hable con nuestro representante de ventas técnico',
    'go Social' => 'Redes Sociales',
    'we Accept' => 'Aceptamos',
    'cards cheques and online transfer' => 'Tarjetas cheques y tranferencias en linea',
    'all Rights Reserved' => 'Todos los derechos reservados ',
    'dedicated Server Hosting' => 'Dedicated Server Hosting',
    'vPS Hosting' => 'VPS Hosting',
    'cloud Hosting' => 'Cloud Hosting',
    'server Colocation' => 'Server Colocation',
    'data Center' => 'Data Center',
    'email Server Hosting' => 'Email Server Hosting',
    'application Hosting' => 'Application Hosting',
    'domain Registration' => 'Domain Registration',
    'sSL Certificates' => 'SSL Certificates',
    'save' => 'salvar'
      
       
];
?>