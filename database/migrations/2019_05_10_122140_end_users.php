<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EndUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sql');
            $table->string('office');
            $table->string('sharepoint');
            $table->string('exchange');
            $table->string('skypebiz');
            $table->string('visualstudio');
            $table->string('dynamics');
            $table->string('project');
            $table->string('rds');
            $table->string('visio');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('machines'); 
    }
}
