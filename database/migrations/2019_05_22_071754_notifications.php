<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
                         if(!Schema::hasTable('notifications')){
        Schema::defaultStringLength(191);
      Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('dcid');
             $table->string('userid');
            $table->string('vm');
               $table->string('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
