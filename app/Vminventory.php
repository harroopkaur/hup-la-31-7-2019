<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vminventory extends Model
{
  protected $fillable = [ 'user_id','dc_id','name','endUser','updatedate','vm','host','cores','procs','dateinstall','sql','office','sharepoint','exchange','skypebiz','visualstudio','dynamics','project','rds','visio'];

public function sub_table()
{
    return $this->hasMany('App\User','id','user_id');
}
}
