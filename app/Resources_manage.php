<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resources_manage extends Model
{
    protected $fillable = [
        'cat_id','title', 'pdf', 'type', 'image', 'doc','content',
    ];

    public function categories()
    {
    	return $this->hasMany('App\Category','id','cat_id');
    }
}
