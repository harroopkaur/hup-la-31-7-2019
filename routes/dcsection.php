<?php

// Route::get('/home', function () {
//     $users[] = Auth::user();
//     $users[] = Auth::guard()->user();
//     $users[] = Auth::guard('dcsection')->user();

//     //dd($users);

//     return view('dcsection.home');
// })->name('home');

	


Route::post('/save', 'dcsectionController\dcsectionController@store');
Route::post('/action', 'dcsectionController\dcsectionController@action');
Route::get('/search','dcsectionController\dcsectionController@search');
 Route::post('/update','dcsectionController\dcsectionController@update');
Route::any('/home','dcsectionController\dcsectionController@show');
Route::post('/savevm','dcsectionController\dcsectionController@savevm');
Route::post('/updatevm','dcsectionController\dcsectionController@updatevm');
Route::post('/import','dcsectionController\dcsectionController@import');

Route::post('/sendemail','dcsectionController\dcsectionController@sendemail');
Route::any('/EnduserManagement','dcsectionController\dcsectionController@enduserdata');

Route::post('/updatevmuser','dcsectionController\dcsectionController@updatevmuser');

Route::any('/export','dcsectionController\dcsectionController@exportdata');


Route::any('/sendemail','dcsectionController\dcsectionController@sendemail');

Route::any('/notification/{id}','dcsectionController\dcsectionController@notiview');

Route::any('/sendmassage/{id}','dcsectionController\dcsectionController@sendmassage');

Route::post('/ajaxRequest', 'dcsectionController\dcsectionController@ajaxRequestPost');

Route::any('/tmpmanage', 'dcsectionController\dcsectionController@templates');
Route::any('/edittmp/{id}', 'dcsectionController\dcsectionController@edittmp');

Route::any('/tmpupdate', 'dcsectionController\dcsectionController@tmpupdate');




/*****************userguide******************************************** */
Route::get('/category','dcsectionController\UserguideController@cat_list');



/***************************resources************************************** */

Route::get('/resources/{id}','dcsectionController\manageresourcesController@resources')->name('resources');

Route::any('clean','dcsectionController\dcsectionController@clean');

// notification la section//

Route::any('/lanoti','dcsectionController\dcsectionController@massage');

Route::any('/lasendsms','dcsectionController\dcsectionController@sendlamassage');

Route::any('/deletelasms/{id}','dcsectionController\dcsectionController@deletelasms');

