<?php

 /*Route::get('/vminventory', function () {
     $users[] = Auth::user();
     $users[] = Auth::guard()->user();
     $users[] = Auth::guard('admin')->user();

   dd($users);

     return view('admin.home');
 })->name('home');*/
 
Route::get('/vminventory','dccontroller@show');

Route::post('/save', 'dccontroller@store');
Route::get('/index','dccontroller@home');
Route::post('/action', 'dccontroller@action');
Route::get('/search','dccontroller@search');
 Route::post('/update','dccontroller@update');

Route::post('/savevm','dccontroller@savevm');
Route::post('/updatevm','dccontroller@updatevm');
Route::post('/sendemail','dccontroller@sendmail');


Route::any('/temp','dccontroller@temp');
Route::any('/temp/{id}','dccontroller@tempup');

Route::post('/tmpsave','dccontroller@tempsave');
Route::any('/tmpupdate','dccontroller@tmpupdate');

Route::any('/tmpview','dccontroller@tmpview');
Route::any('/tmpupdatedc','dccontroller@tmpupdatedc');
Route::any('/tempdc/{id}','dccontroller@tempupdc');

/**********************userguide********************************************* */
Route::get('/category','UserguideController@cat_list');
Route::get('/addcategory','UserguideController@addcategory');
Route::post('/addcategory','UserguideController@insert_value');
Route::get('/editcategory/{id}','UserguideController@editcategory');
Route::post('/editcategory/{id}','UserguideController@update');
Route::get('/deletecategory/{id}','UserguideController@deletecategory');

/***************************resources*************************************** */

Route::get('/resources/{id}','manageresourcesController@resources')->name('resources');
Route::get('/sources/{id}','manageresourcesController@addresources')->name('sources');
Route::post('/sources/{id}','manageresourcesController@insert_data')->name('sources');
Route::post('/delete','manageresourcesController@delete');


// notification////
Route::any('/notification','dccontroller@notiview');
Route::any('/sendlamassage','dccontroller@chatbox');
////end notification section/////////

Route::any('/messages','dccontroller@dcmassage');
Route::any('/converladc/{id}','dccontroller@converdc');

Route::any('/lasendsms','dccontroller@lasendsms');

Route::any('/deletesms/{id}','dccontroller@deletesms');


//////////